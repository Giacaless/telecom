# Come redigere una documentazione tecnica
[[_TOC_]]
***
## Introduzione
Dopo aver sviluppato un progetto (meglio ancora se durante il suo sviluppo) è necessario redigere una *documentazione tecnica*. Per documentazione tecnica si intende un testo, o altro supporto, cartaceo o multimediale, il quale chiarisca in modo corretto e completo come eseguire e riprodurre la procedura svolta. Va ricordato – inoltre – che la documentazione tecnica è l'unica cosa che l'utente finale possiede per poter riprodurre la procedura descritta. Pertanto, ogni passaggio dovrà essere descritto con la massima cura onde evitare confusioni o incertezze in chi legge.

Bisogna descrivere a parole ciò che si è progettato e realizzato, approfondire e puntualizzare le scelte operate che non sono immediatamente visibili o interpretabili nei documenti allegati e nel prodotto finale che si è realizzato. In sintesi, il lettore deve essere guidato passo dopo passo.

In ambito lavorativo la documentazione serve sia per il committente, sia per gli organi che debbono approvare il lavoro, sia per chi deve poi tradurre in pratica il progetto realizzando l'opera.

Pertanto, la documentazione tecnica è molto più di una semplice formalità. Si tratta non solo di scrivere su quanto si è ideato e progettato, ma di un complemento necessario, utile anche per il tecnico: non sempre il direttore di lavori è lo stesso tecnico progettista, e in ogni caso sono molteplici i tecnici che curano la realizzazione di un progetto.

Questo capitolo è scritto per fornire un primo punto di partenza a chi deve affrontare il problema di redigere correttamente (dal punto di vista tecnico) una documentazione tecnica.

## Titolo

È la parte più importante della documentazione. Da esso dipende se il lettore proseguirà nella lettura o la scarterà a priori. Deve essere essenziale e – allo stesso tempo – comunicare il più possibile il contenuto che verrà affrontato.

Come esempio utilizziamo il titolo di questo capitolo:

> Come redigere una documentazione tecnica

e – come primo esercizio – si eseguirà l'analisi logica del testo. Comunicare, infatti, è un'arte che richiede molteplici competenze, prima tra tutte la padronanza della lingua in cui si scrive

* Come = avverbio
* redigere = predicato verbale
* una documentazione tecnica = complemento oggetto

In questo titolo, il complemento oggetto è legato direttamente al verbo senza l'uso di preposizioni: così si semplifica la lettura e la comprensione alla frase.

Si sarebbe potuto evitare l'avverbio iniziale? Sì. Il motivo per cui è stato introdotto è per sottolineare che questo capitolo rientra nella categoria dei cosiddetti how-to[^1].

### Oggetto

L'*oggetto* è da intendersi come un sottotitolo, o meglio come una sua maggiore esplicazione. Sempre relativamente a questo capitolo un oggetto potrebbe essere:

> A uso interno dell'ITIS "Enrico Mattei"

dove il lettore troverà una specifica – o connotazione – ulteriore. Non ci si è ancora calati nei dettagli, ma si sta informando il lettore di cosa ha tra le mani: un manuale rivolto agli studenti dell'Istituto Tenico "Enrico Mattei su come redigere una documentazione tecnica. Quest'operazione è fondamentale affinché il lettore possa decidere se la documentazione gli interessa oppure no.

Un titolo e un oggetto errati, o fuorvianti, possono sì, indurre alla lettura, ma "con l'inganno". Il lettore non utilizzerà più questa fonte di informazioni una seconda volta.

### Alcune regole base per la titolazione

Si è detto che per ogni documentazione il titolo è fondamentale: è l'elemento che per primo la caratterizza e richiama l'attenzione del lettore riassumendo il senso intero del testo che segue. Se è costruito in maniera sbagliata, il lettore non sarà interessato alla prosecuzione della lettura.

È bene ricordare alcune regole base. La regola principale è quella di garantire nel titolo un'informazione immediata, chiara e univoca. Se si intende titolare la notizia della nascita del figlio di Mario Rossi[^2], che si chiamerà Paolo, le vie possibili sono molte. Il titolo più semplice è il seguente:

> Mario Rossi è diventato papà[^3]

Ovviamente, si possono fare molte variazioni, mantenendo fede ai principi di chiarezza e immediatezza, ma giocando con le parole in modo da rendere più piacevole il titolo:

> Fiocco azzurro in casa Rossi
>
> È nato Paolo, festa in casa Rossi
>
> Nuovo nato in casa Rossi, ecco Paolo

È bene – invece – evitare l'errore di una titolazione pomposa, la quale rischia spesso di sviare l'attenzione dalla notizia principale. Un esempio è il seguente:

> Festa in casa Rossi per il nuovo arrivato

In questo caso, infatti, il lettore si chiederà: Qual è la notizia? La festa o il nuovo arrivato? E poi: chi è che è arrivato? Un parente che viveva negli Stati Uniti o un bimbo?

La titolazione, per essere completa, ha bisogno in genere di altri due elementi, l'occhiello e il sommario (detto anche – in gergo giornalistico – catenaccio). L'occhiello è la frase che precede il titolo vero e proprio; il sommario è la parte che lo segue. Occhiello e sommario forniscono ulteriori elementi per la comprensione della notizia. Per esempio:


| Campi     | Contenuto                                                    |
| --------- | ------------------------------------------------------------ |
| Occhiello | Pesa 4 kg, ha i capelli neri e assomiglia alla mamma         |
| Titolo    | **Fiocco azzurro in casa Rossi**                             |
| Sommario  | Paolo è nato ieri al Policlinico. Il parto seguito dall'equipe del dott. Stranamore |


La titolazione completa, occhiello/titolo/sommario deve fornire un quadro esaustivo delle informazioni che seguiranno. Ovviamente, è possibile – anzi, a livello giornalistico, fortemente consigliabile – inserire elementi di dubbio soprattutto nel sommario, per spingere il lettore a proseguire nella lettura.

La titolazione proposta nell'esempio precedente potrebbe comprendere un sommario di questo tipo:

> Paolo è nato ieri sera al Policlinico. E al mattino subito una sorpresa per i genitori

Il lettore si chiederà: una sorpresa per i genitori? Quale sorpresa? E sarà ovviamente spinto a leggere fino a soddisfare la curiosità. La sorpresa per i genitori, in questo caso, potrebbe essere la visita inaspettata da parte di un parente che non si vedeva da vent'anni.

### Introduzione

Questa è la parte dove si parla dell'argomento ancora con circospezione, cosa che trae in inganno la maggior parte degli studenti. Nell'introduzione – o abstract[^4] – ci si focalizza maggiormente sugli aspetti che verranno trattati in seguito, fornendo un quadro esaustivo al lettore. Quanto seguirà dovrà essere riportato nello stesso ordine presentato nell'introduzione.

Supponiamo di dover redigere la documentazione tecnica dal titolo: "*Verifica della legge di Ohm*".

### Cosa scrivere

È importante capire quale argomento si andrà a indagare. Quindi è bene ricordare la cos'è la legge di Ohm, a quali contesti si applica, le eventuali formule inverse (se ritenute significative), come verrà impostato il problema – senza scendere troppo nei dettagli, i quali verranno illustrati in seguito – e ogni altra cosa ritenuta utile.

Il tutto utilizzando l'ordine che seguirà nella documentazione. Come anticipato, il lettore deve essere preso per mano e guidato passo dopo passo nella lettura. Non è possibile dare per scontato che conosca i pensieri di chi scrive.

### Cosa non scrivere

Un esempio di introduzione completamente errata è la seguente:

> Georg Simon Ohm nacque in una famiglia protestante di Erlangen, Brandenburg-Bayreuth, (allora parte del Sacro Romano Impero), figlio di Johann Wolfgang Ohm, un fabbro e Maria Elizabeth Beck, figlia di un sarto di Erlangen. Sebbene i suoi genitori non fossero stati educati formalmente, il padre di Ohm era un uomo piuttosto straordinario che si era educato a un alto livello ed era in grado di dare ai suoi figli un'eccellente educazione attraverso i propri insegnamenti. Con tutti i fratelli e le sorelle di Ohm che erano nati egli avrebbe voluto formare una grande famiglia, ma, come era comune a quei tempi, parecchi bambini morirono nella loro infanzia. Dei sette bambini nati a Johann e Maria Ohm solo tre sopravvissero, Georg Simon, suo fratello Martin, che proseguì per diventare un famoso matematico e sua sorella Elizabeth Barbara. Sua madre morì quando Georg aveva dieci anni.[^5]

I cenni storici possono essere interessanti solo se hanno ricadute sul modo di condurre l'esperimento scientifico che ci si appresta a compiere o verificare.

Nel caso in esempio, la biografia di Georg Simon Ohm non ha nulla a che vedere con il lavoro che verrà condotto in laboratorio (a meno che non si tratti di un laboratorio di Storia).

## Presentazione del problema

Ora che è chiaro di cosa si discuterà, occorre chiarire come questo verrà fatto. Probabilmente, ci si recherà in un laboratorio: quale? Questa domanda sembra banale e retorica ma non lo è. Non si è interessati all'aula in sé, ma al suo contenuto, alle attrezzature, ai dispositivi e ai software eventualmente utilizzati.

È bene quindi redigere un accurato quadro riassuntivo degli strumenti e dei materiali che dovranno essere utilizzati per condurre a termine l'esperimento. Riprendendo l'esempio precedente, una elenco potrebbe essere quello riportato nella tabella seguente.

| Strumento    | Tipo         | Costruzione | Modello     |
| ------------ | ------------ | ----------- | ----------- |
| Alimentatore | Stabilizzato | McPower     | LN-130 pro |
| Voltmetro    | Digitale     | PCE         | PCE-DM 12   |
| Amperometro  | Analogico    | Pierron     | ML2742      |

Con una scheda di lavoro di questo tipo, è possibile dotarsi dei medesimi strumenti di misurazione utilizzati per il lavoro. Questo significa che è auspicabile attendersi simili errori casuali e sistematici.

### Cosa non scrivere

Sono assolutamente da evitare frasi criptiche quali "*abbiamo via, via aumentato la resistenza*". Quale resistenza? Quella dell'insieme dei movimenti politici e militari che in Italia, dopo l'8 settembre 1943, si opposero al nazifascismo nell'ambito della guerra di liberazione italiana? Nel laboratorio di Storia, forse sì.

Purtroppo, con frasi del genere, non si fornisce nessuna indicazione di carattere pratico a chi legge. La fisica studia i fenomeni fisici sotto un profilo quantitativo, non qualitativo. Dunque, servono numeri.

### Cosa scrivere

Un approccio decisamente migliore è il seguente:

> Per l'esperienza eseguita sono state utilizzate le seguenti resistenze, di cui si riporta il valore nominale e il loro valore misurato tramite l'Ohmmetro (il voltmetro digitale è in realtà un multimetro e consente la misurazione di diverse grandezze fisiche, tra le quali la resistenza elettrica).
> Tutte le resistenze utilizzate, in questa esperienza, hanno una potenza massima di 1/4 W e una tolleranza del 5%.

| Resistenza | Valore nominale   | Valore misurato   |
|------------|:-----------------:|:-----------------:|
| N. 1       |  1.0 k$`\Omega`$  | 0.967 k$`\Omega`$ |
| N. 2       |  1.5 k$`\Omega`$  | 1.625 k$`\Omega`$ |
| N. 3       |  2.2 k$`\Omega`$  | 2.357 k$`\Omega`$ |
| N. 4       |  3.3 k$`\Omega`$  | 3.165 k$`\Omega`$ |
| N. 5       |  4.7 k$`\Omega`$  | 4.987 k$`\Omega`$ |
| N. 6       |  6.8 k$`\Omega`$  | 7.012 k$`\Omega`$ |

Così facendo – non solo sarà possibile utilizzare le medesime resistenze – ma anche verificare i risultati ottenuti con i valori effettivi di quest'ultime; valutare se rientrano nel *range* della tolleranza e ogni altra cosa ritenuta opportuna.

A questo punto vengono richiesti alcuni schemi a cui far riferimento.

## Schemi elettrici

Lo schema elettrico mostra il circuito di misurazione così come lo si disegnerebbe – a livello teorico – alla lavagna. In questo schema alcuni dettagli possono essere omessi, per questo motivo diventa importante riportare anche come i vari elementi circuitali sono stati tra loro connessi, senza trascurare nessun dettaglio.

In figura è riportato un ipotetico schema elettrico:

![Ipotetico schema elettrico](https://gitlab.com/Giacaless/telecom/-/raw/master/img/relazione-tecnica/schema-elettrico.png)

Dove vengono evidenziate le connessioni degli strumenti di misurazione (l'amperometro e il voltmetro), la resistenza che verrà via, via cambiata e l'alimentatore stabilizzato.

## Schemi di montaggio

Lo schema di montaggio ha – come anticipato – la funzione di mostrare un'anteprima di come realmente si presenta il circuito. Può essere realizzato mediante un disegno come con una fotografia (se questa è chiara e comprensibile). Non esiste una scelta migliore a priori: è chi scrive che sceglie cosa fare, a seconda della miglior chiarezza espositiva.

Nella figura che segue è riportato lo schema di montaggio relativo al circuito disegnato nella figura sovrastante.

![Schema di montaggio](https://gitlab.com/Giacaless/telecom/-/raw/master/img/relazione-tecnica/schema-montaggio_bb.svg)

I colori convenzionali sono rosso (tensione o corrente positiva) e nero (tensione o corrente negativa o neutra). In questo schema si è utilizzato anche il colore blu per indicare un cavo intermedio al fine di evitare confusioni.

## Approfondimenti

A questo punto – se non lo si è già fatto nelle relazioni precedenti – è bene soffermarsi sugli strumenti di misurazione. In particolare, prontezza, sensibilità, precisione, portata sono alcuni parametri che è bene conoscere e far conoscere al lettore.

Inoltre, l'amperometro analogico (se utilizzato) presenterà un errore di parallasse: come evitarlo? Riguardo a entrambi gli strumenti è comunque bene chiedersi (ed esporre a chi legge) come deve essere impostato il fondo scala per minimizzare gli errori di misurazione.

## Espansioni suggerite

Anche se lo schema elettrico è stato proposto dal docente, ci si potrebbe comunque chiedere cosa accade invertendo l'ordine di inserzione del voltmetro e dell'amperometro[^6]. Oppure non utilizzando il voltmetro e ricorrendo – al posto suo – al voltmetro integrato nell'alimentatore stabilizzato.

Sono tanti gli aspetti che possono essere ulteriormente esplorati, aprendo scenari molto interessanti.

## Tabelle e grafici

Una volta eseguito l'esperimento scientifico questo può essere raccontato sotto due forme: tabelle e grafici.

### Tabelle

Le tabelle servono per riportare i dati in maniera accurata. In questa sede è bene riportare tutte – e sole – le cifre significative.

Per esempio, se un voltmetro digitale riporta la seguente misurazione: V = 7.234 V, nella tabella non è corretto riportare il dato 7.2 V, poiché così facendo si sopprimono due cifre significative (il centesimo e millesimo di Volt) riportate dallo strumento di misurazione. Se lo strumento fornisce un dato numero di cifre significative queste devono essere riportate tutte, zeri inclusi. Un esempio di questo caso è V = 5.340 V, misurazione da non riportare in tabella come 5.34 V, poiché – così facendo – sembrerebbe che le cifre significative siano tre, anziché quattro. L'ultimo zero fa parte della misurazione e, in questo caso, è da considerarsi significativo poiché esprime il valore del millesimo di volt.

Analogamente – nella redazione di una tabella – è altresì errato, in luogo di una misurazione come V = 8.12 V, riportare il dato 8.120 V poiché il millesimo di volt è un dato incognito in quanto non misurato.

In tabella vengono – sovente – riportati anche dati statistici quali errori assoluti, errori relativi, errore medio e deviazione standard. Questi parametri servono – durante il confronto tra dati teorici e dati misurati – a stabilire se la teoria e l'esperienza pratica coincidono, se gli errori sono accidentali o sistematici e altre cose di interesse.

### Grafici

A differenza delle tabelle, i grafici servono a "visualizzare" l'andamento di un fenomeno fisico. È così possibile capire se la relazione tra ingresso e uscita di un sistema è di diretta proporzionalità, inversa proporzionalità, quadratica o altro. Da questo tipo di indagine è più facile risalire al tipo di equazione matematica che lega ingresso e uscita.

Per contro, i grafici non forniscono i dati esatti dei parametri misurati, come in tabella. È importante ricordare quindi che da una tabella può essere ottenuto il suo grafico, ma non è vero il viceversa.

### Tipologie di grafici

Mentre riportare i dati in tabella è relativamente semplice, la stessa cosa non può dirsi per i grafici. Infatti se si lavora con dati discreti si utilizzerà un istogramma (conteggio di eventi che si verificano in dati istanti non continui nel tempo o in intervalli di tempo prestabiliti); invece per dati continui si utilizzerà un diagramma.

In seconda istanza, se i dati variano molto il loro valore (per esempio, da $`10^{-3}~\text{V}`$ a $`10^3~\text{V}`$, per evitare che i valori elevati rendano di difficile (o impossibile) visualizzazione quelli troppo piccoli, appiattendo il grafico, è fortemente consigliato l'uso della scala logaritmica, la cui proprietà è appunto avere una uguale ampiezza per ogni decade. Così facendo i piccoli valori saranno visibili come i grandi valori.

## Conclusioni

Anche questo paragrafo spesso viene male interpretato, purtroppo. Non si tratta di scrivere se tutto è andato bene o come previsto. Le conclusioni devono fornire al lettore un singolo messaggio: da questo lavoro di laboratorio si è acquisita questa competenza o quest'altra esperienza.

Vale lo stesso se l'obiettivo preposto nel titolo della documentazione non è stato raggiunto. Non è importante commettere errori, importante davvero è saper individuare una giustificazione valida a essi.

A esempio: se si ottengono valori discordanti da quelli attesi, ma i componenti utilizzati, una volta misurati, forniscono valori differenti da quelli nominali; oppure gli strumenti di misura risultano starati a fronte di un confronto con strumenti di maggior fiducia; ancora: si è eseguita l'analisi una seconda volta cambiando tecnologia di misurazione, e gli esempi potrebbero continuare. In tutti questi casi si forniscono ottimi dati al lettore per non commettere i medesimi errori.

Descrivere gli insuccessi non significa esporsi allo scherno o rendersi ridicoli. La scienza è piena di insuccessi che si sono tramutati come un boomerang nella fortuna di chi li ha commessi: il *post-it* è figlio di una colla che funzionava malissimo; inoltre si narra che un tale – di nome Galileo – nel 1616 abbandonò il tribunale dell'Inquisizione mormorando "...eppur si muove".

L'unica cosa importante è che le vostre conclusioni siano migliori di quelle di Erasmus Wilson, presidente dello *Stevens Institute of Technology*, quando nel 1879 dichiarò: "Quest'invenzione dell'energia elettrica è un fallimento totale"; o di Lord Kelvin, il cui nome dovrebbe ricordare qualcosa, famoso fisico britannico e presidente della *Royal Society* il quale nel 1895 dichiarò: "È impossibile che qualcosa più pesante dell'aria possa volare".

## Postfazione (per non concludere)

### Impostare la struttura del testo

La struttura del testo scientifico che vi apprestate a scrivere è variabile a seconda del tipo di documento (tesi di laurea, documentazione tecnica, verbale di riunione, riassunto di un articolo, brevetto, eccetera) ma si possono dare alcune indicazioni sempre valide. La prima indicazione è che un testo scientifico deve essere scritto in forma impersonale: "è stata effettuata una visita presso l'azienda XY", e non in prima persona "abbiamo visitato l'azienda XY"; oppure "si può concludere quindi che i risultati...", e non "possiamo quindi concludere che i risultati...".

La prima pagina deve sempre riportare chiaramente l'autore del testo, il titolo del lavoro e le eventuali informazioni aggiuntive legate all'ambito nel quale il documento è stato scritto. Per esempio, una documentazione tecnica su una visita didattica presso un’azienda riporterà: autore, titolo, corso di studi, materia, data della visita e nome del docente. Queste informazioni possono essere in testa alla prima pagina (per testi brevi, 2/3 pagine), oppure, per testi più lunghi, (oltre le dieci pagine) essere riportate da sole, su una prima pagina di intestazione; in questo caso il testo vero e proprio inizia nella seconda pagina.

È sempre fondamentale numerare le pagine del documento e riportare in ognuna (in intestazione o piè pagina) il proprio nome e il titolo del documento. Se il testo è di media lunghezza e/o molto articolato, è opportuno inserire anche un indice. In genere l'indice si posiziona nella prima o nella seconda pagina (se la prima è di sola intestazione). L'indice riporta i titoli dei capitoli e dei paragrafi con il numero della pagina corrispondente.

Il testo deve avere una struttura. Ciò significa che le diverse parti che lo compongono (indice, introduzione, parte generale, risultati, conclusioni, bibliografia, allegati) devono essere immediatamente identificabili, sia grazie all'uso di opportuni formati, sia grazie all'organizzazione del testo; la cosa migliore è utilizzare una struttura gerarchica, disponibile in ogni elaboratore di testi. Questa procedura semplificherà anche la creazione dell'indice, che può essere generato automaticamente.

### Un'informazione efficace ed essenziale

È bene fare molta attenzione nel costruire una frase lineare e comprensibile. La frase contorta: "*la ricerca e l'innovazione tecnologica è distintiva della qualità del prodotto finito ed è la chiave per un migliore futuro per quelli che lavorano con finestre di terminale*" diventa, spendendo qualche parola in più: "*la ricerca e l'innovazione tecnologica sono distintivi della qualità del prodotto finito e rappresentano la speranza di un futuro migliore per tutte le persone che si trovano a lavorare con finestre di terminale*".

Non siate mai avari nelle cancellature; parole e frasi superflue ricorrono abitualmente negli scritti scientifici, ma è facile riconoscerle ed eliminarle: "*appezzamenti esaminati in successione **l'uno dopo l'altro...***"; "*la Figura 1 schematizza i risultati **in forma grafica***"; "*la concimazione in copertura è stata **totalmente** abolita*"; "*il dolore è di natura **essenzialmente** cardiaca*".

La lunghezza di un testo non conta, il timore di non aver scritto abbastanza è puerile. Molto più grave è essere ridondanti e prolissi, tediando il lettore; uno scritto scientifico ha il dovere di essere chiaro, conciso, essenziale. Le documentazioni tecniche non si vendono a peso. A questo scopo frasi brevi e lineari, con pochi incisi, sono di più agile lettura rispetto a frasi lunghe o con otto subordinate inanellate l'una di seguito all'altra prima della reggente. Prestate un'attenzione particolare all'uso corretto della punteggiatura, spesso mal gestita. Molte persone trattano la punteggiatura come l'acne: se non c'è, meglio.

Non si deve temere di fare affermazioni chiare; smussare le teorie con varie forme dubitative servirà solo ad appesantire il discorso.
Evitate frasi logore o abusate: "*Sulla base dei dati della letteratura e degli studi effettuati presso il nostro laboratorio...*"; "*Alla luce dei risultati ottenuti sembrano necessari ulteriori studi per confermare il ruolo delle pagine dinamiche...*" e luoghi comuni "*La ricerca scientifica ha fatto passi da gigante*".

### La scelta delle parole

Chiarezza e accuratezza sono i criteri cardine nella scelta delle parole da usare nel testo. Siate certi dell'esatto significato delle parole utilizzate, senza abbracciare le cattive consuetudini della lingua colloquiale; sempre più spesso diverse parole sono scritte in modo scorretto o usate impropriamente, a esempio perché contaminate da un termine omologo straniero, così da creare ibridi che non trovano riscontro in alcun vocabolario:

| Sì                       | No                     |
|--------------------------|------------------------|
| Effettuare una scansione | Scannerizzare/scandire |
| Gestire la complessità   | Complessizzare         |
| Redigere un referto      | Refertare              |
| Aggirare                 | Bypassare              |
| Aggiornare/Ottimizzare   | Upgradare              |
| Inoltrare                | Forwardare             |

Di fatto, il linguaggio scientifico prevede già numerosi termini mutuati dalle lingue straniere, perché privi di un esatto corrispondente in italiano. Bisogna fare attenzione a distinguere tra termini universalmente accettati e termini di uso poco comune, i quali possono essere ignoti al lettore medio e appesantiscono il testo.

Tanto più si usano parole elaborate e ricercate, tanto maggiore è il rischio di commettere errori. In ogni caso è bene tenere sempre a portata di mano un buon vocabolario e un dizionario scientifico/tecnico aggiornato. Se ben consultati, questi due strumenti possono essere di grande aiuto e fornire soluzioni a molti dubbi.

Non fate alcun affidamento sul correttore automatico dell'editor di testo; ci sono molti termini errati che i correttori automatici non rilevano. Per i puristi della lingua italiana:

| NO              | Sì                                               |
|-----------------|--------------------------------------------------|
| test            | prova/saggio/analisi/esame/inchiesta                  |
| outcome         | esito/decorso                                         |
| pattern         | schema/forma/aspetto                                  |
| evidence-based | basata sulle prove di efficacia/basata sulle evidenze |

### Tabelle e grafici

Altre note in merito all'uso di tabelle, immagini e grafici in un testo scientifico. Questi devono essere sempre codificati da un numero e da un titolo. Se vengono riportati dati di fonte esterna (dati che non sono frutto di autonoma elaborazione) nel titolo va sempre citata la fonte:

* Tabella 3. Piovosità media in Urbino nel decennio 2000–2010. (Dati Regione Marche, 2014).
* Grafico 5. Tenore proteico della dieta di bovine da latte in funzione del livello produttivo (rielaborato da Ketelaars e van der Meer, 2001).

La tabella o il grafico devono avere sempre una citazione nel testo, che li presenta e/o li commenta:

* I principali circuiti integrati sono riportati in Tabella 3.

* Le principali aziende italiane (Tabella 7) sono le cosiddette PMI.

Ciò significa che essi devono essere posizionati nel testo con ragionevole contiguità con il testo che li illustra (compatibilmente con i vincoli di impaginazione).

I dati che si intende inserire nel testo devono essere presentati in forma tabellare oppure in forma grafica, possibilmente non sotto entrambe le forme; una tabella e un grafico non devono mai presentare gli stessi dati. Sia nelle tabelle, sia nei grafici devono essere sempre esplicitate le unità di misura di tutti i dati riportati.

## La bibliografia

Il testo scientifico spesso fa riferimento a concetti o dati sperimentali derivati da altri testi, articoli, capitoli di libri. Tali riferimenti devono essere sempre corredati del relativo riferimento bibliografico. Nel brano che segue sono presenti due diverse forme di citazione bibliografica, quella tra parentesi e quella nel testo corrente:

"Per la gestione della fertilizzazione sono state molto usate in passato le curve di risposta della coltura a differenti livelli di concimazione (**Boschi et al., 1982**; **Toderi e Giordani, 1982**; **Spallacci, 1983**; **Grignani e Reyneri, 1987**; **Grignani, 1995**; **Briffaux, 1999**). Esse però, come evidenziato da **Giardini (1992)** e **Grignani (1995)**, hanno un forte limite: dipendono strettamente dalle condizioni del suolo, dalle condizioni climatiche e meteorologiche, dalla cultivar, dalla tecnica colturale."

Nel caso che le citazioni siano più di una, esse vanno ordinate in ordine cronologico, e a parità di anno di pubblicazione, in ordine alfabetico.

Ogni riferimento bibliografico presente nel testo, in una tabella o in un grafico va poi esplicitato in modo dettagliato al fondo del testo, in un'apposita sezione bibliografica. Qui sono indicati quattro esempi, relativi alla citazione da un articolo scientifico, da una tesi di dottorato, da un capitolo di un libro e dagli atti di un convegno:

> Alessandroni, G. et al. *A Study on the Influence of Speed on Road Roughness Sensing: The*
> *SmartRoadSense Case*. Sensors 2017, 17(2), p. 305.

> Alessandroni, G., *Analisi e Modelli per il Monitoraggio del Manto Stradale*, PhD Thesis,
> University of Urbino, 2016, pp. 169.

> Alessandroni, G. *Dov’erano i pacifisti? Dodici mesi di movimento pacifista*. In: Guerra e
> mondo—Annuario geopolitico della pace, ed. Altreconomia, 2004, pp. 15–38.

> Alessandroni, G. et al. *Sensing road roughness via mobile devices: A study on speed influence*. Image and Signal Processing and Analysis (ISPA), 2015 9th International Symposium on, 7–9 September 2015, Zagreb. pp. 73–75.

## Note

[^1]: Gli *how-to* sono documenti – maggiormente diffusi in ambito scientifico e informatico – affini al campo della conoscenza procedurale. La parola how-to può essere tradotta con "come fare per..." e denota un tono informale della trattazione. Sono, generalmente, monotematici e pensati per aiutare i meno esperti tralasciando dettagli che potrebbero interessare solo il pubblico con conoscenze più approfondite riguardo all'argomento di trattazione.
[^2]: Un esempio volutamente estraneo alle telecomunicazioni per assumere un carattere generale e valido in ogni contesto.
[^3]: Anche se non è stato scritto, a questo punto dovrebbe essere chiaro a tutti che il titolo non vuole la punteggiatura. Fa eccezione il punto interrogativo.
[^4]: L'*abstract* è il sommario di un documento senza aggiunta di interpretazione e valutazione. Si limita a riassumere, in un dato numero di parole, gli aspetti fondamentali del documento esaminato. Solitamente ha forma "indicativa-informativa"; presenta notizie sulla struttura del testo e il percorso elaborato dall'autore. La norma internazionale ISO 214–1976 definisce il termine "abstract" come "*una rappresentazione abbreviata e accurata dei contenuti di un documento senza aggiungere interpretazione e valutazione e senza distinguere il destinatario dell'abstract*'', mentre per la norma 5127–1981 (versione italiana del 1987) lo si definisce come "*riassunto del contenuto di un documento in forma abbreviata, senza interpretazione né critica*".

[^5]: Tratta da https://it.wikipedia.org/wiki/Georg_Ohm. Questo esempio viene riportato poiché sono state corrette – con valutazioni che si possono facilmente immaginare – relazioni con questo contenuto, le quali, oltre a essere un plagio, non aggiungono nulla di significativo, anzi: sviano il lettore.
[^6]: La cosiddetta inserzione dello strumento "a monte" e "a valle".