# Principi di multiplazioni numeriche

[[_TOC_]]

***

## Multiplazione

La **multiplazione**, (in inglese *multiplexing*), è la tecnologia mediante la quale la capacità trasmissiva disponibile in un collegamento in uscita, viene condivisa tra diversi canali in ingresso, combinando più segnali – analogici o numerici – in un solo segnale; il quale verrà trasmesso mediante un singolo mezzo trasmissivo.

Questo significa che il mezzo trasmissivo è condiviso, facendovi transitare più flussi informativi (segnali telefonici, dati, flussi video, ecc.), garantendo – allo stesso tempo – la separabilità dei dati in ricezione (la, cosiddetta, ortogonalità dei segnali).

Un **multiplexer** (spesso abbreviato in *mux*) è un dispositivo che condivide la capacità disponibile di un unico collegamento fra più canali trasmissivi, mediante la multiplazione. Suddivide tra diversi circuiti la banda disponibile di un generico mezzo trasmissivo (fibra ottica, doppino in rame, ecc.). La sua azione è svolta sul segnale ed è indipendente dallo specifico mezzo utilizzato.

Un multiplexer ha $`2^{N}`$ ingressi, $`N`$ ingressi di selezione e una singola uscita.

In ricezione, il dispositivo è complementare e si chiama **demultiplexer** (o *demux*), permette di separare i diversi canali trasmissivi originali.

Viceversa, il demultiplexer ha un singolo ingresso, $`N`$ ingressi di selezione e – di conseguenza – $`2^{N}`$ uscite.

![Semplice multiplexer e demultiplexer telefonico a divisione di tempo (Fonte: Tony R. Kuphaldt)](https://gitlab.com/Giacaless/telecom/-/raw/master/img/multiplazioni/Telephony_multiplexer_system.gif)

### Commutazione di circuito

Una trasmissione si dice a **commutazione di circuito** (o a *multiplazione deterministica*) quando una frazione fissa della capacità trasmissiva totale viene assegnata a ciascun canale in ingresso.

Questo significa che ogni utenza disporrà di un canale trasmissivo dedicato, con la garanzia di poter utilizzare *sempre* tutta la sua capacità trasmissiva a ogni richiesta di servizio. Per contro, implica che tale canale risulterà occupato anche in assenza di trasmissione.

L'esempio classico è la conversazione telefonica, dove si utilizza un circuito dedicato su una giunzione tra centrali. L'utente chiamante compone un numero, la rete telefonica lo processa, collegando una serie di circuiti (una volta meccanici, ora elettronici) fino al destinatario della chiamata (instradazione della connessione); se il destinatario risponde, i due utenti possono dialogare utilizzando il canale; quando il chiamante abbassa il ricevitore, la rete libera le risorse impegnate (abbattimento della connessione).

L'esempio chiarisce un aspetto fondamentale: la linea telefonica, nel corso della conversazione, risulterà occupata anche se gli utenti resteranno nel più alto silenzio.

<img alt="Esempio di instradamento di una conversazione telefonica mediante la commutazione di circuito." src="https://gitlab.com/Giacaless/telecom/-/raw/master/img/multiplazioni/network.png"  width="600">

### Commutazione di pacchetto

Una trasmissione avviene in **commutazione di pacchetto** (o a *multiplazione statistica*) quando il flusso di informazioni è diviso in “pacchetti”, di lunghezza limitata, contrassegnati da un'intestazione e un piè di pagina con informazioni sia per il loro riconoscimento all'interno del flusso dati, sia per la definizione della destinazione dei pacchetti stessi.

L'intera capacità trasmissiva disponibile viene impegnata per la trasmissione dei pacchetti. Se vi sono più pacchetti da trasmettere contemporaneamente, questi vengono memorizzati in un buffer, subendo così un ritardo di accodamento e rischiando di essere scartati in caso di esaurimento della memoria disponibile per la coda.[^1]

![Nella commutazione di pacchetto il mezzo trasmissivo è condiviso, come avviene in figura,e ogni pacchetto dati lo occupa a turno.](https://gitlab.com/Giacaless/telecom/-/raw/master/img/multiplazioni/commutazione-pacchetto.png)

La commutazione di pacchetto nasce negli anni ’60, con i fondi dell'ARPA[^2], dove vennero sperimentate tecnologie innovative per collegare alcuni computer fra loro attraverso le linee telefoniche. Questa tecnologia fa sì che i dati in transito su una linea telefonica siano divisi in più parti, ognuna con il proprio indirizzo di destinazione. Questo permette di lasciare la rete libera per altre comunicazioni quando non si ha nulla da comunicare.

La maggior parte delle tecnologie di multiplazione di pacchetto non permettono di garantire prestazioni prestabilite, per questo motivo si dicono *statistiche*. La multiplazione di pacchetto è tipica delle reti di calcolatori, in ragione del tipo di traffico impulsivo che queste generano.

Si possono distinguere i seguenti tipi:

* **commutazione di messaggio**: l'intero messaggio da trasmettere viene inviato senza segmentazione. Questo può portare a ritardi non prevedibili in caso di messaggi grosse dimensioni. È obsoleta e ha più che altro un valore concettuale;
* **commutazione di pacchetto** propriamente detta: il messaggio viene segmentato in pacchetti di lunghezza limitata, trasmessi indipendentemente. In una rete complessa, è possibile che i pacchetti seguano percorsi differenti, vengano persi o consegnati fuori ordine alla destinazione. L'esempio tipico è il protocollo TCP/IP;
* **commutazione di trama** (*frame switching*): questo termine è un sinonimo di commutazione di pacchetto, viene utilizzato in riferimento a tecnologie di livello *datalink* nel modello OSI, per esempio l'*ethernet*[^3];
* **commutazione di cella**: in questo caso il pacchetto ha lunghezza fissa, viene riempito con dati inutili se il messaggio da trasmettere ha dimensioni inferiore. La dimensione fissa della cella permette ottimizzazioni negli apparati di commutazione. L'esempio tipico è l'ATM[^4];
* **commutazione di circuito virtuale**: come dice il nome, questa tecnica permette di emulare alcune caratteristiche di una rete a commutazione di circuito, pur utilizzando la multiplazione statistica. Per far questo, prima di stabilire una comunicazione tra due calcolatori, deve essere stabilita una connessione logica tra i due nodi (detta circuito virtuale), definendo tra l’altro il percorso che i pacchetti dovranno seguire. A questo percorso, su ciascun collegamento verrà associato un numero, e i commutatori dovranno solo associare a questo identificatore di circuito una porta ed un identificatore di circuito in uscita. Questo permette di garantire la consegna in ordine dei pacchetti, e facilita la realizzazione di funzioni di qualità di servizio. Questa caratteristica è propria di reti come ATM o *Frame Relay*[^5];
* **commutazione di etichetta** (*label switching*): questa tecnica viene utilizzata per costruire su una rete a commutazione di pacchetto più reti virtuali indipendenti e reciprocamente impermeabili, per esempio da dedicare a diversi gruppi di lavoro in un’azienda oppure a diversi clienti di un provider. A ciascun pacchetto viene aggiunta un'etichetta che lo identifica come appartenente a una particolare rete virtuale, e potrà essere consegnato solo se il destinatario appartiene alla medesima rete virtuale. Tra gli esempi, le VLAN su ethernet ([IEEE 802.11](http://www.ieee802.org/11/)) e *Multi Protocol Label Switching* (MPLS): un meccanismo che crea reti private virtuali su tecnologie eterogenee, tipicamente utilizzato dai provider.

### Differenze sostanziali

In caso di commutazione di circuito, se si riesce a stabilire la connessione con il destinatario, allora questa sarà garantita. Con la commutazione di pacchetto, invece, non si ragiona più in termini di “qual è la probabilità di realizzare una connessione?”, ma – più semplicemente – “qual è la probabilità che un pacchetto arrivi a destinazione?”.

I pacchetti – infatti – possono “perdersi” lungo il tragitto (coda piena, *time out*). In questo caso il destinatario richiede la ritrasmissione del pacchetto mancante.

L’ordine di precisione nella rilevazione dei possibili errori nell’*internet checksum* è di 1 bit ogni 65536 bit, quindi la probabilità di un errore ogni $`2^{16}-1`$ bit (circa $`3\cdot 10^{-5}`$), valore ritenuto accettabile poiché gli errori a livello di frame IP sono da considerarsi rari: i frame IP – infatti – sono a loro volta incapsulati in frame con un controllo più efficace (come il frame ethernet [IEEE 802.3]( http://www.ieee802.org/3/) il quale esegue il controllo di errore usando un CCITT CRC a 32 bit).

### Multiplazione e teoria delle code

Lo studio di entrambi i sistemi di multiplazione sfrutta ampiamente la teoria delle code. Per esempio:

* nella *commutazione deterministica* si valuta la probabilità che una telefonata non possa essere servita a causa della congestione del fascio di linee o di un commutatore[^6]. I serventi sono le linee disponibili in un fascio, i clienti le telefonate, i tempi di servizio la durata delle telefonate;
* nella *commutazione statistica* si studia la probabilità che un pacchetto possa essere perso perché la coda di trasmissione è piena, le caratteristiche statistiche del ritardo di accodamento a cui andrà incontro. I serventi sono le linee di trasmissione, i clienti i pacchetti, i tempi di servizio quelli necessari per la trasmissione dei pacchetti.

Il comportamento statistico delle code in una rete determina le sue caratteristiche di qualità di servizio.

### Multiplazione inversa

Come dice il nome, la multiplazione inversa (nota anche come demultiplazione, o *demultiplexing*) è il meccanismo per cui la capacità di più collegamenti viene aggregata per costruire un canale trasmissivo di capacità superiore. Questo comporta – normalmente – protocolli per distribuire i dati da trasmettere sui collegamenti disponibili, per ricomporre poi i dati ricevuti sui collegamenti aggregati nell’ordine corretto, e gestire i guasti e il ritorno in servizio di parte dei collegamenti.

Esempi di multiplazione inversa sono l’aggregazione di più collegamenti IEEE 802.3 tramite il protocollo LACP ([IEEE 802.3ad](http://www.ieee802.org/3/ad/)), o l’aggregazione di link ATM a bassa velocità (T1 a 1.5 Mbps o E1 a 2 Mbps) per ottenere un collegamento di capacità superiore (6/12 Mbps o 8/16 Mbps) *Inverse multiplexing over ATM* (IMA).

### Multiplazione nei protocolli di trasporto

I protocolli di trasporto utilizzano il servizio di un protocollo di rete, che trasporta pacchetti da un *host* a un altro, per realizzare comunicazioni tra processi applicativi.

È possibile che sia necessario stabilire più connessioni contemporanee tra applicazioni sulla stessa coppia di host. Per questo, i protocolli di trasporto hanno strumenti di multiplazione e demultiplazione (tipicamente le porte) per specificare a quale conversazione appartenga un segmento e quindi a quale vadano consegnati i dati ricevuti.

Esempi classici sono il TCP, *Transmission Control Protocol* e l’UDP, *User Datagram Protocol*.

<img alt="Animazione che mostra come avviene l'instradamento dei pacchetti. Fonte: Wikimedia, User: Oddobotz" src="https://gitlab.com/Giacaless/telecom/-/raw/master/img/multiplazioni/Packet_Switching.gif"  width="600">

### Un esempio di instradamento di un pacchetto

Richiedendo l'invio di un pacchetto di dati all'MIT di Boston, partendo dalla Berkley University (California), il risultato è stato il seguente:

```bash
traceroute to allspice.lcs.mit.edu (18.26.0.115), 30 hops max
 1 helios.ee.lbl.gov (128.3.112.1) 0 ms 0 ms 0 ms
 2 lilac-dmc.Berkeley.EDU (128.32.216.1) 19 ms 19 ms 19 ms
 3 lilac-dmc.Berkeley.EDU (128.32.216.1) 39 ms 19 ms 19 ms
 4 ccngw-ner-cc.Berkeley.EDU (128.32.136.23) 19 ms 39 ms 39 ms
 5 ccn-nerif22.Berkeley.EDU (128.32.168.22) 20 ms 39 ms 39 ms
 6 128.32.197.4 (128.32.197.4) 59 ms 119 ms 39 ms
 7 131.119.2.5 (131.119.2.5) 59 ms 59 ms 39 ms
 8 129.140.70.13 (129.140.70.13) 80 ms 79 ms 99 ms
 9 129.140.71.6 (129.140.71.6) 139 ms 139 ms 159 ms
10 129.140.81.7 (129.140.81.7) 199 ms 180 ms 300 ms
11 129.140.72.17 (129.140.72.17) 300 ms 239 ms 239 ms
12 * * *
13 128.121.54.72 (128.121.54.72) 259 ms 499 ms 279 ms
14 * * *
15 * * *
16 * * *
17 * * *
18 ALLSPICE.LCS.MIT.EDU (18.26.0.115) 339 ms 279 ms 279 ms
```

Si noti che il i passi 12, 14, 15, 16 e 17 sono dispersi o non inviano il codice ICMP di *time-out*, o inviano un ttl troppo piccolo per poterci raggiungere. Nei punti 14–17 sono in esecuzione dei codici “MIT C” Gateway che non inviano il time-out.

Dio solo sa cosa sia successo al passo 12.[^traceroute]

## Multiplazione a divisione di Frequenza (FDM)

La **multiplazione a divisione di frequenza**[^7], è una tecnica di condivisione delle risorse trasmissive di un canale di comunicazione, dove l'intero mezzo trasmissivo disponibile viene suddiviso in sottocanali, ognuno costituito da una banda di frequenza, separati grazie ad un piccolo intervallo, detto *banda di guardia*.

Questo rende possibile la condivisione dello stesso canale da parte di diversi dispositivi e utenti, i quali possono così comunicare contemporaneamente senza incorrere nella mutua interferenza.

![Modello concettuale della multiplazione FDM](https://gitlab.com/Giacaless/telecom/-/raw/master/img/multiplazioni/fdm.png)

Nella multiplazione a divisione di frequenza, i flussi tributari sono trasportati in diverse porzioni dello spettro disponibile sul canale. Si sfrutta la proprietà che i segnali elettromagnetici a frequenze diverse non interagiscono tra di loro.

Le *bande di guardia* sono porzioni di frequenza che rimangono inutilizzate. Sono necessarie poiché i filtri passa banda non sono ideali, ma – soprattutto nelle code – provocano la perdita di dati. Per questo motivo si parla spesso di **banda lorda** (la banda di stazione sommata alla banda di guardia) e di **banda netta** (la sola banda di stazione).

Il principio è esattamente lo stesso che consente alle diverse emittenti radio o televisive di trasmettere contemporaneamente “via *etere*”. Ogni stazione, per esempio, radiofonica trasmette il suo segnale associandolo a una frequenza (detta “portante”) differente. Il ricevitore (nell’esempio, la radio in ascolto) si “sintonizza” sulla portante in questione ed estrae solo il flusso desiderato, demultiplandolo dall’insieme dei flussi trasmessi da tutte le stazioni radio.

### ADSL, un esempio di multiplazione FDM

Nella trasmissione di un segnale ADSL[^8] il modem è solito dividere la banda transitante sul “doppino” telefonico (il cavo in rame che connette l’utente alla centrale telefonica) in tre parti:

1. una prima (4 kHz) destinata alle conversazioni telefoniche;
2. una seconda (da 1 Mbps a 20 Mbps) destinata al traffico dati in uscita;
3. una terza (da 2 Mbps a 100 Mbps) riservata al traffico dati in ingresso.

L'ADSL è quindi una multiplazione asimmetrica perché la banda destinata al traffico in uscita è diversa da quella destinata al traffico in entrata.

![Ripartizione della banda nel protocollo ADSL](https://gitlab.com/Giacaless/telecom/-/raw/master/img/multiplazioni/adsl.png)

### Altri esempi di multiplazione FDM

Nello spettro radio, i diversi sistemi operano a divisione di frequenza tra di loro (come il *broadcast* televisivo, le reti della telefonia mobile GSM fino alla seconda generazione, i radar, ecc.)

Nei sistemi trasmissivi ottici, molti canali trasmissivi (per esempio 40 o 100) possono essere multiplati su una stessa fibra ottica operando con divisione di frequenza, che viene in questo caso chiamata WDM: *Wavelenght Division Multiplexing*.

È una tecnologia molto raffinata per sfruttare al massimo la potenzialità delle fibre ottiche, inviando segnali con diversi colori e ricordando che un colore diverso è una frequenza diversa.

## Multiplazione a divisione di tempo (TDM)

La **multiplazione a divisione di tempo**[^9], è una tecnica di multiplazione, di condivisione di un canale di comunicazione secondo la quale ogni dispositivo ricetrasmittente ottiene a turno l'uso esclusivo dello stesso, per un lasso di tempo prefissato (tipicamente il periodo di campionamento è pari a $`T_{C}=125\mu\text{s}`$).

Tale tempistica discende dalla frequenza di campionamento, pari al doppio della banda lorda del segnale, ovvero:
```math
B_{L}=\frac{1}{2T_{C}}=\frac{1}{2\cdot 125\mu\text{s}}=4~\text{kHz}
```
Naturalmente non è pensabile utilizzare un singolo doppino telefonico per ogni singola comunicazione possibile, le combinazioni sarebbero teoricamente illimitate. Meglio destinare i canali telefonici via, via a chi ne fa richiesta e liberarli al termine della chiamata.

La multiplazione a divisione di tempo è la tecnica duale della FDM nel dominio del tempo.

### Modulazione codificata di impulsi (PCM)

La **modulazione codificata di impulsi**[^10] è un metodo di rappresentazione digitale di un segnale analogico. La PCM è ampiamente utilizzata nei sistemi di telefonia, ma su essa si basano anche diversi standard video. La codifica PCM è impiegata per agevolare le trasmissioni digitali in formato seriale.

Non si tratta di una vera e propria modulazione, poiché i segnali non vengono “spostati” nel dominio della frequenza, ma compressi, riducendo il tempo di trasferimento.

Questo significa che se la prima centralina telefonica – come in effetti avviene – raccoglie trenta utenze e cui vanno aggiunte due di servizio (una di sincronismo ed una di controllo), utilizzate per instradare correttamente le comunicazioni, e il segnale viene campionato a 8 bit , si ha che – per non rallentare il segnale – questo ora dovrà viaggiare più velocemente. Si ha:
```math
T_{Canale}=\frac{T_{C}}{32}=\frac{125~\mu\text{s}}{32}=3,9~\mu\text{s}
```
Dove la trama è modulata con tecnologia TDM. Per ogni canale si trasmettono 8 bit , quindi il tempo dedicato alla trasmissione del singolo bit è pari a quello di un canale diviso 8:
```math
T_{bit}=\frac{T_{Canale}}{8}=\frac{3,9~\mu\text{s}}{8}=488~\text{ns}
```
Infine, trasmettendo 32 canali, con 8.000 campioni al secondo, dove ogni canale contiene 8 bit, si evince che la velocità di trasferimento dell'informazione numerica PCM risulta:
```math
v_{bit}=Canali \cdot Campioni \cdot Bit = 32 \cdot 8.000 \cdot 8 = 2.048~\text{kbps}
```
Il lettore attento avrà sicuramente notato che il valore ottenuto nella (4) è l'inverso di quello ottenuto nella (3). Questo dato è uno standard europeo[^11].

Nella pratica, questa è nota come *gerarchia PCM di primo livello*.

### PCM con gerarchie superiori al primo

Il PCM primario ha una velocità di trasferimento dell'informazione pari a 2.048 kbps. Si analizzeranno ora, come si costruiscono le gerarchie successive.
Si procede costruendo unificando 4 flussi primari costituiti con 32 canali a 8 bit. Così facendo si ottiene un sistema multiplato di secondo livello gerarchico avente velocità:
```math
v_{bit}=4\cdot 2.048~\text{kbps} = 8.192~\text{kbps}
```
Il numero di bit complessivi nel tempo di trama $`T_{C}=125~\mu\text{s}`$ diventa quindi pari a:
```math
n=256\cdot 4 = 1024~\text{bit}
```
Dove, il tempo assegnato a ciascun bit è:
```math
T_{bit}=\frac{T_{C}}{n}=\frac{3,9~\mu\text{s}}{1024~\text{bit}}=122~\text{ns}
```
A onor del vero la (5) approssima per difetto la velocità del fascio dei canali, poiché per instradare
correttamente 120 canali non sono sufficienti 8 canali di controllo, ma occorre inserire alcuni bit
ausiliari. Si arriva così a 8.448 kbps.

| Canali | Sigla | Velocità (Mbps) | Velocità teorica |
| -----: | :---: | --------------: | ---------------: |
|   30   |  E1   |      2.048      |                  |
|  120   |  E2   |      8.448      |      8.192       |
|  480   |  E3   |     34.368      |      32.768      |
| 1.920  |  E4   |     139.264     |     131.072      |
| 7.680  |  E5   |     565.148     |     524.288      |
| 30.720 |  E6   |    2,5 Gbps     |    2.097.152     |

### ISDN a banda stretta

La rete ISDN[^12] è una rete numerica integrata nelle tecniche e nei servizi. Consente all'utente di avere un unico tipo di accesso per tutti i tipi di servizi che può ottenere dalla rete.

Affinché questo sia possibile occorrono alcuni prerequisiti:

* tecniche di trasmissione e commutazione completamente numeriche;

* nodi della rete con capacità multiservizio;
* interfacce utente/rete standardizzate.

La rete ISDN presenta molti vantaggi, in primo luogo *per l'utente*, come un unico attacco di rete e terminali multifunzione; *per il gestore* si ha una razionalizzazione delle risorse ed una gestione ottimizzata; infine *per l'industria* si verifica una concentrazione di investimenti e opportunità di nuovi servizi/mercati.

Non è tutto oro quel che luccica. La rete ISDN presenta un costo maggiore per gli utenti e richiede investimenti da parte di gestori e industrie.

## Multiplazione a divisione di codice (CDM)

La **multiplazione a divisione di codice**[^13], è una tecnologia molto utilizzata nelle comunicazioni radio.

Uno dei concetti base della comunicazione dati è l'idea di consentire a più trasmettitori di inviare informazioni simultaneamente in un unico canale di comunicazione. Questo permette a diversi utenti di condividere una banda di frequenze. Questo concetto si chiama multiplazione a divisione di codice. La CDM utilizza una tecnologia ad ampio spettro e di una particolare tecnica di codifica (dove a ogni trasmettitore viene assegnato un codice) per consentire a più utenti di essere multiplexati sullo stesso canale fisico.

Il CDM sincrono sfrutta le proprietà matematiche dell'ortogonalità tra vettori, i quali rappresentano le stringhe di dati. Ad esempio, la stringa binaria $`1011`$ viene rappresentato dal vettore $`(1,~0,~1,~1)`$. I vettori possono essere moltiplicati per ottenere il loro prodotto scalare, sommando i prodotti dei loro rispettivi componenti. Quando il prodotto scalare è nullo, allora i due vettori si dicono ortogonali tra loro.

![Un esempio di quattro segnali numerici mutualmente ortogonali.](https://gitlab.com/Giacaless/telecom/-/raw/master/img/multiplazioni/segnali-mutualmente-ortogonali.png)

Facendo un confronto con le multiplazioni TDM (dove si suddivide il tempo) e l'FDM (dove si suddividono le frequenze), il CDM – invece – è una forma di segnale a spettro espanso, poiché il segnale modulato a divisione di codice ha una larghezza di banda di dati molto più elevata rispetto ai dati che vengono comunicati.

### Codificatore CDM

Il **codificatore CDM** è costituito essenzialmente da due parti: la prima divide la sequenza di bit generata da un codificatore (opzionale) in $`N`$ repliche (se la sequenza era $`+1~+1~-1`$, ecc. ora sarà $`N`$ volte $`+1`$, $`N`$ volte $`+1`$, $`N`$ volte $`-1`$, ecc.); la seconda parte moltiplica ogni replica generata per un termine $`d[n]`$ , detto codice di canalizzazione di lunghezza $`N`$. Ogni bit del codice di canalizzazione ($`+1`$ o $`-1`$) viene moltiplicato con le repliche della sequenza iniziale, così da generare un codice in base alla sequenza di partenza. In ricezione il segnale potrà essere decodificato solo da chi possiede il codice di canalizzazione esatto.

Un sistema di telecomunicazioni basato su CDM offre anche maggiori protezione sul fronte della sicurezza della comunicazione poiché solo la conoscenza delle parole di codice permette di poter effettuare la demultiplazione del rispettivo canale tributario in ingresso all'interno dell'intero flusso multiplato.

### Applicazioni

Il CDM è il protocollo di accesso a canale condiviso di comunicazione più diffuso nelle reti wireless, nelle tecnologie cellulari di terza generazione (3G), che funzionano secondo lo standard UMTS[^14], e della trasmissione dei messaggi dei satelliti GPS.

### Il sistema UMTS

Il sistema UMTS, con l'utilizzo della multiplazione CDM a banda larga, supporta un tasso di *trasferimento*[^15] massimo teorico di 21 Mbps, sebbene gli utenti delle attuali reti abbiano a disposizione un tasso di trasferimento limitato a 384 kbps utilizzando dispositivi R99 e fino a 7,2 Mbps con dispositivi HSPDA nelle connessioni in *download*.

Le applicazioni tipiche dell'UMTS, sono tre: voce, videoconferenza, trasmissione dati a pacchetto. A ognuno di questi tre servizi è assegnato uno specifico tasso di trasferimento: per la fonia 12,2 kbps, per la videoconferenza 64 kbps, per trasmissioni dati 384 kbps.

Valori decisamente superiori ai 14,4 kbps di un singolo canale GSM con correzione di errore. L'UMTS è quindi in grado, potenzialmente, di consentire per la prima volta l'accesso di dispositivi mobili al World Wide Web.

### Assegnazione delle licenze UMTS in Italia

In Italia il governo Amato, nel 2000, ha indetto un'asta per l'assegnazione di 5 licenze UMTS. Le cinque licenze UMTS sono state assegnate ai cinque concorrenti rimasti in gara (Omnitel, Wind, Andala, TIM e Ipse) dopo il ritiro di Blu. Dall'asta italiana si è ricavato un quarto di quella tedesca, un terzo di quella britannica e grossi dubbi sulla capacità del governo di controllare il mercato della telefonia.



## Dal tam-tam al tom-tom (GPS)

Il **sistema GPS**[^16] fornisce la posizione esatta e l'ora a un numero illimitato di persone, giorno e notte, in qualsiasi parte del mondo. Il sistema si compone di tre parti: i satelliti in orbita intorno alla Terra; stazioni di controllo e di monitoraggio sulla Terra, e il ricevitore GPS di proprietà degli utenti finali. I satelliti GPS trasmettono segnali provenienti dallo spazio, i quali vengono raccolti e identificati dai ricevitori GPS. Ogni ricevitore GPS fornisce quindi la localizzazione tridimensionale (latitudine, longitudine e altitudine), oltre al tempo.

Il GPS è diventato un pilastro dei sistemi di trasporto in tutto il mondo, fornendo la navigazione per l'aviazione, il trasporto terrestre e le operazioni marittime.

Originariamente – a partire dal febbraio 1978 fino a essere  completamente operativo nel 1993 – il sistema GPS è stato creato e realizzato dal *Dipartimento della Difesa statunitense* (USDOD) ed è stato realizzato con 24 satelliti; attualmente – i satelliti in orbita – sono 33.

Oltre al sistema GPS altri sono in in fase di sviluppo o in uso. Il russo *Global Navigation Satellite System* (GLONASS) è stato implementato da parte e per l'esercito russo sin dal 2007. Oltre a questo sono previsti altri due sistemi di navigazione: il *Compass* cinese e il sistema di posizionamento *Galileo* dell'Unione Europea per usi civili.

Di seguito una tabella riassuntiva:

| Sistema      | BeiDou                              | Galileo                             | GLONASS       | GPS                                  |
| :----------: | :---------------------------------- | :---------------------------------- | :------------ | :----------------------------------- |
| Proprietario | Cina                                | Unione Europea                      | Russia        | Stati Uniti                          |
| Satelliti    | 28                                  | 22                                  | 24            | 33                                   |
| Precisione   | 10 m (pubblica)<br>0,1 m (criptata) | 1 m (pubblica)<br>0,01 m (criptata) | 4,5 m – 7,4 m | 5 m (pubblica)<br>< 0,1 m (criptata) |

### Limiti fisici del sistema GPS

Una delle fonti di errore più significativi è il sincronismo dell'orologio del ricevitore GPS. A causa del valore elevato della velocità della luce, $`c`$[^17], le distanze stimate dal ricevitore GPS per i satelliti sono molto sensibili agli errori nell'orologio del ricevitore GPS. Ad esempio un disallineamento di $`1~\mu\text{s}`$ genera un errore – in termini di stima della posizione – di circa $`300~\text{m}`$. Questo suggerisce che l'orologio del ricevitore debba essere estremamente accurato e preciso per ottenere dati affidabili.

Naturalmente questi orologi non possono essere alloggiati nei ricevitori, ne renderebbero proibitivi i costi. Pertanto, il problema si sposta sui satelliti, dove troviamo degli orologi atomici[^18]; sulle stazioni di controllo non si può spendere così tanto: lì troviamo orologi sì molto accurati, ma che si sincronizzano periodicamente con quelli satellitari.

Gli orologi satellitari – a causa delle elevate velocità dei satelliti – sono affetti dalle conseguenze della teoria della relatività. Pertanto, a causa degli effetti combinati della velocità relativa, la quale rallenta il tempo sul satellite di circa $`7~\mu\text{s}`$ al giorno, e della minore curvatura spaziotemporale a livello dell'orbita del satellite, che lo accelera di $`45~\mu\text{s}`$, il tempo sul satellite scorre leggermente più veloce rispetto a terra, causando un anticipo di circa $`38~\mu\text{s}`$ al giorno, rendendo necessaria una autocorrezione da parte dell'elettronica di bordo.[^19]

### Restrizioni all'uso civile del sistema GPS

Il sistema GPS non può essere usato in qualsiasi condizione. Vi sono alcuni limiti che il governo statunitense ha imposto agli utenti civili.
Tutti i ricevitori GPS in grado di funzionare oltre i $`18~\text{km}`$ di altitudine *e* $`515~\text{m/s}`$ , sono classificati come armi per le quali sono richiesti titoli di esportazione dal Dipartimento di Stato degli Stati Uniti. Questi limiti sono imposti affinché i sistemi GPS non siano usati per guidare missili balistici. Tali vincoli, tuttavia, non impedirebbero l'uso nei missili cruise poiché la loro altitudine e velocità sono simili a quelli degli aerei di linea.

Oltre a tali vincoli vengono introdotti errori intenzionali nella misurazione dal Ministero della Difesa degli Stati Uniti. In pratica viene intenzionalmente modificata l'accuratezza dell'orologio a scapito della precisione della misurazione. Questo fa sì che i sistemi GPS militari siano più precisi di quelli civili.

### GNSS: un ricevitore multisatellitare

In un articolo molto interessante[^20] viene descritto lo stato attuale dei principali sistemi satellitari di navigazione globale (GNSS[^21]): il ben noto sistema statunitense e ampiamente utilizzato NAVSTAR GPS, il sistema russo GLONASS e il sistema europeo di navigazione satellitare Galileo, compatibile e interoperabile con il GPS. Vengono esaminate alcune informazioni generali, la storia e lo stato attuale, dopodiché vengono studiate le prestazioni di questi sistemi di navigazione satellitare. Nell'articolo i sistemi vengono trattati separatamente senza affidarsi a fonti esterne come *Satellite Based Augmentation System* (SBAS) che trasmettono le informazioni a livello di correzioni su vaste aree di misurazioni (WAAS, EGNOS, MSAS), o tecnologie assistite con unità di misura inerziale (IMUS[^22]) o le tecnologie di telecomunicazione come GSM o UMTS.

Nella trattazione i sistemi GNSS in stato di sviluppo – allora – non avanzato sono stati volutamente omessi, come la Cina, la quale ha già realizzato un suo sistema analogo agli SBAS, detto BeiDou e sembra essere pronto a sviluppare un autonomo GNSS chiamato *Compass*. Un satellite è già in orbita, ma le informazioni su segnali e servizi utilizzati non sono stati resi noti. La Cina, assieme alla Nigeria, ha realizzato un satellite SBAS chiamato NigComsat-1 per fornire informazioni di correzione per il continente africano. Un sistema simile con il satellite geostazionario GAGAN è prevista per l'India.

Anche il Giappone ha istituito un sistema di tre satelliti per garantire una buona visibilità di satelliti GNSS nella regione occidentale del Pacifico.

Le prestazioni sul rilevamento del posizionamento GNSS variano da $`30~\text{m}`$, a $`50~\text{cm}`$ e infine arrivano a circa $`1~\text{mm}`$. Tutti questi livelli di precisione sono stati dimostrati. Tuttavia, non è sempre chiaro come si è operato per ottenere il livello di precisione descritto. In generale, ci sono tre diversi tipi di osservazione e trasformazione in modalità GNSS: punto di posizionamento unico, misurazioni differenziali (in codice) e osservazioni del fasore. È dimostrato che – per motivi di compatibilità – GPS e Galileo utilizzano segnali simili e strutture di segnali analoghi, quindi lo sviluppo del ricevitore può essere effettuato in modo analogo per entrambi i sistemi. A causa dalla diversa multiplazione di frequenza (FDM) la situazione con GLONASS cambia. In un ricevitore GLONASS si necessita di una maggiore complessità circuitale.

### Come funziona il GPS

Il principio di funzionamento del GPS è semplice, ed è la trilaterazione: un punto nello spazio può essere identificato con l'intersezione di quattro sfere.

![Un esempio visivo della costellazione GPS in moto con la Terra in rotazione. Il numero di satelliti in vista da un dato punto sulla superficie terrestre, in questo esempio a 45° N, cambia nel tempo. Fonte: Wikimedia, Autore: El pak](https://gitlab.com/Giacaless/telecom/-/raw/master/img/multiplazioni/ConstellationGPS.gif)

In figura vi è un esempio visivo della costellazione GPS in orbita, con la Terra in rotazione. Il numero di satelliti totali è 24, ma il numero di satelliti visibili in un dato punto della superficie terrestre, in questo esempio a $`45°~\text{N}`$, cambia nel tempo.

Per semplicità di calcolo lo studio verrà eseguito nel caso bidimensionale, sostituendo le sfere con circonferenze: per identificare univocamente un punto nel piano occorrono – pertanto – tre circonferenze.

Con una singola circonferenza potremmo dedurre la seguente informazione:

> Siamo a $`215~\text{km}`$ da Napoli

informazione che può essere rappresentata così:

![Siamo a 215 km da Napoli](https://gitlab.com/Giacaless/telecom/-/raw/master/img/multiplazioni/GPS-napoli.png)

dove noi siamo posizionati sulla circonferenza, ma senza sapere dove. Attraverso una seconda osservazione otteniamo un'altra informazione:

> Siamo a $`271~\text{km}`$ da Firenze

Rappresentando graficamente anche questo dato, si ottiene la seguente situazione:

![Siamo a 271 km da Firenze](https://gitlab.com/Giacaless/telecom/-/raw/master/img/multiplazioni/GPS-firenze.png)

Ora – per quel che riguarda la nostra posizione – possiamo trovarci solo in due luoghi: le intersezioni delle circonferenze. Per capire dove siamo effettivamente occorre una terza informazione:

> Siamo a $`80~\text{km}`$ da Roma

![Siamo a 80 km da Roma](https://gitlab.com/Giacaless/telecom/-/raw/master/img/multiplazioni/GPS-roma.png)

La ricerca è terminata. Per i curiosi possiamo svelare l'arcano: siamo a Rieti.

Nella realtà – come detto in precedenza – il calcolo viene fatto nello spazio tridimensionale (usando quattro rilevazioni, quindi *almeno* quattro satelliti). In luogo dei cerchi si utilizzano delle sfere, le quali si intersecano tra loro fino ad identificare un unico punto.

## Ma la Terra è tonda o piatta?

Mi spiace, non volevo essere io a darvi questa notizia ma… la terra non è piatta, non è appoggiata placidamente sul dorso di un elefante e se correte rischiate di cadere – questo sì – ma se attraversate le Colonne d'Ercole (sapete cosa sono vero?) non precipiterete verso l'ignoto.

![Tratta LHR-SYD-JFK](https://gitlab.com/Giacaless/telecom/-/raw/master/img/multiplazioni/LHR-SYD-JFK.gif)

Nel 2018 la compagnia aerea Qantas ha effettuato un giro del mondo, con un volo di linea, tramite Airbus A350-1000 Partendo dall'aeroporto di Heathrow, con scalo a Sydney, sino al JFK di New York. Agli osservatori più acuti non sarà sfuggito un dettaglio: la tratta del velivolo.

| Da                           | A                            | Distanza      |
| ---------------------------- | ---------------------------- | ------------- |
| LHR (51°28'39"N 00°27'41"W)  | SYD (33°56'46"S 151°10'38"E) | 17.016 km     |
| SYD (33°56'46"S 151°10'38"E) | JFK (40°38'24"N 73°46'43"W)  | 16.013 km     |
| **TOTALE**                   |                              | **33.028 km** |

Perché l'aereo non ha percorso la distanza più breve tra due punti: la linea retta? Non avrebbe risparmiato? No, perché l'ortodromia è la più acerrima nemica dei terrapiattisti!

Nella navigazione – non importa se stradale, marittima o aerea – se si considera accettabile l'approssimazione della Terra come sferica, si può affermare che un aeromobile o un'imbarcazione naviga per ortodromia quando, nell'andare da un punto a un altro della superficie terrestre, percorre l'arco di circonferenza minimo che li congiunge. 

## Tonda, tonda?

Tonda, tonda, tonda proprio no. Diciamo che è uno sferoide. Per via della rotazione attorno sé stessa, e la relativa forza centrifiga, è *dilatata all'equatore* (o - se preferite - *schiacciata ai poli*, anche se, fisicamente parlando, meno corretto).

![Raggi terrestri equatoriali (a) e polari (b) medi come definiti nella revisione del Sistema Geodetico Mondiale del 1984.](https://gitlab.com/Giacaless/telecom/-/raw/master/img/multiplazioni/WGS84.png)

In figura vengono mostrati i raggi terrestri equatoriali (*a*) e polari (*b*) medi come definiti nella revisione del 1984 del Sistema Geodetico Mondiale, noto come **World Geodetic System 84**, o - più brevemente WGS84. Su tale modello si basano tutti i sistemi di navigazione, satellitari e non.

## Note

[^1]: Da qui il principale problema del terzo millennio: il *buffering*, il fenomeno di attesa durante lo *streaming* audio o video via internet.
[^2]: ARPA è l'acronimo di *Advanced Research Projects Agency*, un'agenzia del Dipartimento della Difesa degli Stati Uniti.
[^3]: L’*Open Systems Interconnection* (meglio noto come Modello ISO/OSI) è uno standard stabilito nel 1978, il quale stabilisce una pila di protocolli divisi in sette livelli: fisico, *datalink*, rete, trasporto, sessione, presentazione, applicazione.
[^4]: L’ATM (*Asynchronous Transfer Mode*) incapsula il traffico in celle a lunghezza fissa (53 byte) invece che in pacchetti a lunghezza variabile. È stata progettata agli inizi degli anni ’90 e lanciata con una fortissima spinta in quanto avrebbe dovuto soddisfare le esigenze di networking unificando voce, dati, TV via cavo, telex, ecc., in un unico sistema integrato.
[^5]: Il Frame Relay (o *Frame-mode Bearer Service*) è una tecnica di trasmissione a commutazione di pacchetto, introdotta dalla ITU-T a partire dalla raccomandazione I.122 del 1988. Può essere pensato come una linea virtuale affittata tra due punti, tra i quali si possono inviare frame di lunghezza variabile. Rispetto a X.25 fornisce un servizio minimale; infatti, non fornisce informazioni sull'avvenuta ricezione e non fornisce controllo di flusso. È meno costosa di altre tecnologie WAN come ATM o T-lines (link dedicati o SONET).
[^6]: La rete telefonica nazionale è dimensionate in modo da accettare al massimo il 15% del traffico totale teorico. Questo perché non è possibile dimensionare una rete pensando al potenziale valore di picco. Un esempio classico lo si ha a Natale o capodanno quando i telefoni sono congestionati e non è sempre possibile raggiungere il destinatario. Più seriamente parlando, in caso di catastrofi come terremoti: la Protezione Civile utilizza tecnologie diverse dalla telefonia per comunicare, così da garantirsi un ponte radio sempre attivo.
[^traceroute]: Dalla manualistica del comando [traceroute](https://www.freebsd.org/cgi/man.cgi?query=traceroute&sektion=8&manpath=FreeBSD+6.2-RELEASE).
[^7]: La multiplazione a divisione di frequenza è più conosciuta come *Frequency Division Multiplexing* (dal corrispondente termine di lingua inglese), il cui acronimo è FDM.
[^8]: Acronimo anglosassone di *Asymmetric Digital Subscriber Line*.
[^9]: La multiplazione a divisione di tempo è più conosciuta come *Time Division Multiplexing* (dal corrispondente termine di lingua inglese), il cui acronimo è TDM.
[^10]: La modulazione codificata di impulsi è più conosciuta come *Pulse Code Modulation* (dal corrispondente termine di lingua inglese), il cui acronimo è PCM.
[^11]: Negli Stati Uniti, invece, lo standard è 1544 kbps, per 24 canali telefonici.
[^12]: Acronimo anglosassone di *Integrated Services Digital Network*.
[^13]: La multiplazione a divisione di codice è più conosciuta come *Code Division Multiplexing* (dal corrispondente termine anglosassone), il cui acronimo è CDM.
[^14]: Dall'acronimo anglosassone *Universal Mobile Telecommunications System*.
[^15]: Spesso indicato col suo acronimo anglosassone *transfer rate*.
[^16]: Acronimo anglosassone di *Global Positioning System*.

[^17]: La velocità della luce, indicata tradizionalmente con la lettera $`c`$, dal latino *celeritas*, “velocità”, nel vuoto ha un valore pari a 299˙792˙458 m/s, tipicamente approssimato a 300˙000 km/s.
[^18]: Un orologio atomico misura il tempo con un errore di un secondo ogni tre milioni di anni. Accettabile.
[^19]: Quest'osservazione fornisce un'ulteriore prova dell'esattezza della teoria einsteniana in un'applicazione nel mondo reale. L'effetto relativistico rilevato è infatti esattamente corrispondente a quello calcolabile teoricamente, almeno nei limiti di accuratezza forniti dagli strumenti di misurazione attualmente disponibili.
[^20]: Fonte: [Institut für Photogrammetrie – Universität Stuttgart](https://phowo.ifp.uni-stuttgart.de/publications/phowo07/)
[^21]: Dall'acronimo anglosassone *Global Navigation Satellite Systems*.
[^22]: Dall'acronimo anglosassone *Inertial Measurement Units*.