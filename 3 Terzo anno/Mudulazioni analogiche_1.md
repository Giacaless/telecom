# Modulazioni Analogice

[[_TOC_]]
***

## Modulazione di ampiezza

La modulazione di ampiezza, in sigla AM[^2], è una tecnologia utilizzata principalmente per trasmettere segnali audio utilizzando radiofrequenze e – in passato, in Italia fino al luglio 2012 – per trasmettere la componente video del segnale televisivo.

Consiste nel variare l'ampiezza del segnale che si intende utilizzare per la trasmissione (detto portante) in maniera direttamente proporzionale all'ampiezza del segnale che si intende trasmettere (modulante). Pertanto, la forma d'onda segnale modulato, conserverà sempre la stessa frequenza della portante.

È piuttosto semplice da realizzare, per questo motivo è stata utilizzata agli albori delle trasmissioni radio. Ma, soprattutto, il demodulatore AM – il componente che serve per decodificare il segnale modulato – si può realizzare con due soli componenti elettrici: un diodo e un condensatore. Questa caratteristica ha determinato l'immenso successo della modulazione AM.

I suoi principali inconvenienti sono l'estrema sensibilità ai disturbi e alle condizioni di propagazione, poiché qualsiasi disturbo si somma direttamente al segnale che si sta trasmettendo; oltre alla scarsa efficienza che richiede l'uso di potenze maggiori per coprire le stesse distanze di un segnale modulato in frequenza.

### Teoria

Per semplicità di calcolo e dimostrazione, si suppone il segnale modulante $`v_m(t)`$ periodico, con pulsazione pari a $`\omega_m = 2 \pi f_m`$. Così facendo si ottiene:

```math
v_m(t) = V_m \cos (\omega_m t + \varphi_m).
```

![Andamento del segnale modulante nel dominio del tempo.](https://gitlab.com/Giacaless/telecom/-/raw/master/img/modulazioni-analogiche/am-modulante.png)

Il segnale in figura, per una più agevole semplicità dimostrativa, nel seguito verrà considerato con $`\varphi_m = 0`$, ovvero in fase con in segnale modulante. Una generalizzazione è semplice, ma – allo stadio attuale – appesantirebbe soltanto la spiegazione pertanto viene così semplificata.

Per quanto riguarda il segnale portante – il quale deve avere una frequenza molto maggiore del segnale modulante – ovvero $`\omega_p \gg \omega_m`$, si avrà:

```math
v_p(t) = V_p \cos (\omega_p t).
```

![Andamento del segnale portante nel dominio del tempo.](https://gitlab.com/Giacaless/telecom/-/raw/master/img/modulazioni-analogiche/am-portante.png)

Il raffronto tra il segnale modulante e il segnale portante non evidenzia chiaramente cosa si intende per "molto maggiore". Le due immagini sono così riportate per motivi di chiarezza espositiva. Per "molto maggiore" si intende una grandezza fisica di almeno tre o quattro ordini di grandezza superiore all'altra, come evidenziato in questo scatto fotografico.

<img src="https://gitlab.com/Giacaless/telecom/-/raw/master/img/modulazioni-analogiche/am-oscilloscopio.jpg" alt="Segnale modulato in ampiezza all'oscilloscopio." width="30%" />

La modulazione di ampiezza si ottiene mediante due passaggi: un moltiplicatore ed un sommatore, come riportato in figura:

```mermaid
stateDiagram
  Modulante      --> Moltiplicatore
  Portante       --> Moltiplicatore
  Moltiplicatore --> Sommatore
  Portante       --> Sommatore
  Sommatore      --> AM
```

Il segnale modulato in ampiezza attraverso il modulatore di figura assume la seguente espressione matematica: 

```math
v_{AM}(t) = V_p \cos (\omega_p t) + K_a V_m \cos (\omega_m t) \cos (\omega_p t) = \left(V_p + K_a V_m \cos (\omega_m t) \right) \cos (\omega_p t).
```

Il segnale modulato in ampiezza presenta una costante $`K_a`$ che verrà spiegata in seguito. Essa è determinata dal moltiplicatore dei segnali portante e modulante e assorbe il valore $`V_p`$.

![Segnale modulato in AM.](https://gitlab.com/Giacaless/telecom/-/raw/master/img/modulazioni-analogiche/am-modulato.png)

Avendo posto $`\omega_p \gg \omega_m`$, in un periodo del segnale modulante sono contenute un numero molto elevate di oscillazioni del segnale portante. Questo è importante per la ricostruzione del segnale, il quale deve non solo essere trasmesso, ma anche ricevuto e – pertanto – decodificato.

Ora, raccogliendo $`V_p`$, l’espressione del segnale modulato, può essere riscritta come segue: 

```math
v_{AM}(t) = V_p \left( 1 + \frac{K_a V_m}{V_p} \cos(\omega_m t) \right) \cos(\omega_p t),
```

dove – una volta posto $`m_a = \frac{K_a V_m}{V_p}`$ – dall'equazione precedente, si ottiene questa forma più compatta: 

```math
v_{AM}(t) = V_p \left( 1 + m_a \cos(\omega_m t) \right) \cos(\omega_p t).
```

Il termine, molto importante,

```math
m_a = \frac{K_a V_m}{V_p},
```

prende il nome di indice, o profondità, di modulazione[^3] e deve verificarsi $`m_a \le 1`$ affinché l'inviluppo[^4] del segnale modulato abbia lo stesso andamento dell'informazione da trasmettere.

Nel caso in cui si verifichi $`m_a \ge 1`$ si dice che il segnale $`v_{AM}(t)`$ è in condizione di *sovramodulazione*. In questo caso vengono introdotte distorsioni nell'inviluppo del segnale modulato che non consentono – in ricezione – di ricostruire il segnale modulante $`v_{m}(t)`$. Valori tipici della profondità di modulazione sono pari a $`m_a \simeq 0.4 = 40\%`$.

### Spettro di frequenza di un segnale AM

Lo spettro di frequenza del segnale modulato rappresenta l'ampiezza di ogni componente del segnale. Infatti, ogni segnale periodico è scomponibile in una somma di segnali sinusoidali[^5] quindi anche il segnale modulato è una somma di segnali sinusoidali. Per questo motivo è possibile studiare la modulazione ipotizzando che il segnale modulante sia sinusoidale: ogni altro segnale sarà un insieme di segnali sinusoidali, la dimostrazione di base resterà invariata.

L'espressione del segnale modulato in ampiezza, può essere riscritta più agevolmente come segue:

```math
v_{AM}(t) = V_p \cos(\omega_p t) +  V_p m_a \cos(\omega_m t) \cos(\omega_p t),
```

tuttavia – inquest'espressione, essendoci un prodotto di coseni – è difficile stabilire con esattezza lo spettro del segnale modulato.

Per risolvere questo problema si applica la seconda formula di Werner[^6], la quale recita: 

```math
\cos\alpha\cos\beta = \frac{1}{2}\left[ \cos(\alpha+\beta) + \cos(\alpha-\beta)\right].
```

La seconda formula di Werner può essere dimostrata in diversi modi. Uno dei più semplici è applicare le formule di addizione e sottrazione al coseno al secondo membro della stessa.

Sostituendo la seconda formula di Werner nel segnale modulato in AM e riordinando i termini che si ottengono in ragione della frequenza (partendo dalla frequenza minore, sino alla frequenza maggiore), si ottiene:

```math
v_{AM}(t) = \frac{V_p m_a}{2} \cos\left[(\omega_p-\omega_m)t\right] + V_p \cos(\omega_p t) + \frac{V_p m_a}{2} \cos\left[(\omega_p+\omega_m)t\right].
```

Dall'equazione precedente si nota che un segnale modulato in AM è costituito dalla portante (il termine centrale della sommatoria) e da due componenti cosinusoidali dette righe o – più in generale – bande laterali inferiori e superiori. Tali bande laterali sono *speculari*, ovvero presentano il medesimo contenuto informativo.

Pertanto, la larghezza di banda (o banda di frequenza) è pari a:

```math
B_{AM} = (f_p + f_m) - (f_p - f_m) = 2f_m,
```

dove $`f_m`$ è la frequenza del segnale modulante e $`f_p`$ la frequenza del segnale portante. Questo significa che la banda occupata da un segnale modulato in ampiezza non dipende dalla frequenza del segnale portante, ma unicamente dalla frequenza del segnale modulante.

### Trasmissione con modulazione AM in radiofrequenza

La larghezza di banda di un segnale modulato in AM è pari a $`B_{AM} = 2f_m`$, dove $`2f_m`$ è la frequenza massima del segnale in oggetto. Nelle trasmissioni radiofoniche il segnale modulante è il suono, il cui campo di frequenza si estende tra 20 Hz ed i 20 kHz circa. La larghezza del canale AM di un segnale sonoro, quindi, dovrebbe occupare una banda pari a $`B_{AM} = 40\text{ kHz}`$. Se si desidera aumentare il numero dei canali da trasmettere simultaneamente – a parità di banda totale disponibile – è necessario ridurre la larghezza di banda da assegnare a ciascun canale (a scapito della qualità del segnale).

![Spettro di un generico segnale modulato in ampiezza.](https://gitlab.com/Giacaless/telecom/-/raw/master/img/modulazioni-analogiche/am-spettro.png)

Per quel che riguarda il nostro Paese e l'Unione Europea, si è stabilito – attraverso accordi internazionali[^7] – di fissare la banda di ogni singola stazione a $`B_{AM} = 9\text{ kHz}`$. Nella radiodiffusione in onde medie, le trasmissioni AM sono allocate nella gamma di frequenze comprese tra i 526.5 kHz ed i 1,616.5 kHz.

#### Esercizio *Calcolo del numero massimo di radio AM*

Conoscendo la banda di frequenza riservata alle stazioni AM e a una singola stazione AM, calcolare il numero $`N`$ di stazioni massimo. 

```math
N = \frac{F_{AM_{max}}-F_{AM_{min}}}{B_{AM}} = \frac{1,616.5\text{ kHz} - 526.5\text{ kHz}}{9\text{ kHz}} = \frac{1,090\text{ kHz}}{9\text{ kHz}} \simeq 121.
```

### Potenza e rendimento di un segnale AM

Si indichi con $`R`$ la resistenza d'uscita del circuito modulante. La potenza complessiva $`P_t`$ di un segnale AM è pari alla somma di quella associata al segnale portante $`P_p`$ oltre quella delle due bande laterali, inferiore $`P_{bi}`$ e superiore $`P_{bs}`$, ovvero: 

```math
P_t = P_{bi} + P_p + P_{bs}.
```

Si noti che le tre potenze, costituenti la potenza totale, sono state scritte in ragione della loro componente in frequenza: partendo dalla frequenza minore, sino alla frequenza maggiore.

Ora, ricordando che $`V = R \cdot I`$ e $`P = V \cdot I`$, dalle quali è facile desumere $`P = \frac{V^2}{R}`$, si ha che la potenza di un segnale modulato in AM può essere riscritta in una forma più chiara e leggibile, sfruttando l'indice di modulazione $`m_a`$: 

```math
P_t = P_p\left( \frac{m_a}{2} \right)^2 + P_p + P_p\left( \frac{m_a}{2} \right)^2,
```

poiché le potenze sono proporzionali al quadrato delle tensioni e la resistenza d'uscita è la medesima per tutte le componenti.

Pertanto tale potenza può essere riscritta, più semplicemente, come segue: 

```math
P_t = P_p\left(1 + 2\frac{m_a^2}{4} \right) = P_p\left(1 + \frac{m_a^2}{2} \right).
```

Per quanto riguarda il rendimento $`\eta`$ della modulazione si procede eseguendo il rapporto tra la potenza associata a una banda laterale e quella totale. Pertanto, si ha: 

```math
\eta = \frac{P_{bs}}{P_t} = \frac{P_p \frac{m_a^2}{4}}{P_p\left(1 + \frac{m_a^2}{2} \right)} = \frac{m_a^2}{4 + 2 m_a^2}.
```

#### Esercizio *Rendimento massimo della modulazione AM*

Sapendo che $`m_a \le 1`$, è possibile porre nel'espressione del rendimento $`m_a = 1`$, ottenendo: 

```math
\eta_{max} = \frac{m_a^2}{4 + 2 m_a^2} = \frac{1}{4 + 2} = \frac{1}{6} \simeq 0. 167 = 16.7 \%.
```

Il basso rendimento si giustifica considerando che la maggior parte della potenza è associata alla portante, la quale non contiene informazione, inoltre il segnale viene trasmesso due volte e solo una banda laterale è significamene informativa.

#### Esercizio *Rendimento tipico della modulazione AM*

Per svolgere questo esercizio occorre ricordare che il valore tipico dell'indice di modulazione è pari a $`m_a = 0.4`$ (40%). In questo caso si ottiene:

```math
\eta_{tipico} = \frac{m_a^2}{4 + 2 m_a^2} = \frac{0.4^2}{4 + 2 \cdot 0.4^2} = \frac{0.16}{4 + 2 \cdot 0.16} = \frac{0.16}{4.32} \simeq 0. 0370 = 3.70 \%.
```

Questo è il rendimento tipico della radiodiffusione in modulazione di ampiezza.

## Note

[^1]: Per questa ragione, in alcuni testi, la trasformata di Fourier viene, sovente, chiamata *integrale di Fourier*.
[^2]: Dall'omologo termine inglese *Amplitude Modulation*.
[^3]: La profondità di modulazione o indice di modulazione indica quanto un segnale è modulato. Solitamente viene indicato in percentuale. Il valore di m può variare tra 0 (0%) e 1 (100%); oltre il 100% si presenta l'effetto di distorsione producendo un disturbo che prende il nome di sovramodulazione.
[^4]: Insieme di curve tangenti ad una famiglia di curve. Nella modulazione AM viene utilizzato per ricostruire il segnale modulate.
[^5]: Ogni segnale periodico può essere scomposto come sommatoria di segnali sinusoidali: in questo caso si parla di sviluppo in serie di Fourier. Per segnali non periodici – avendo questi uno spettro continuo – si passa dalla sommatoria all'integrale e si parla di trasformata di Fourier
[^6]: In trigonometria, le formule di Werner permettono di trasformare prodotti di funzioni trigonometriche di due angoli in somme e differenze di funzioni trigonometriche.
[^7]: Per l'Italia occorre consultare il [Piano Nazionale di Ripartizione delle Frequenze (PNRF)](http://www.sviluppoeconomico.gov.it/images/stories/documenti/radio/PNRF_27_maggio_2015.pdf), pubblicato sul supplemento ordinario n. 33 alla Gazzetta Ufficiale del 23 giugno 2015 n. 143, il quale costituisce un vero e proprio piano regolatore dell'utilizzo dello spettro radioelettrico in Italia.
