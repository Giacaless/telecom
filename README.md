<div align="center">
<img alt="Logo ITIS 'Enrico Mattei'" src="img/P_itis_mattei.png" />

### Telecomunicazioni per informatica
</div>

# Indice generale

## Prerequisiti

[Come redigere una relazione tecnica](0 Prerequisiti/documentazione-tecnica.md)

## Terzo anno

[Modulazioni analogiche](3 Terzo anno/Mudulazioni analogice.md)

## Quarto anno

[Principi di multiplazioni numeriche](4 quarto anno/multiplazioni.md)


## Any Other Business