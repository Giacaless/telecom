# Modulazioni analogiche (2)

[[_TOC_]]

***

## Modulazione di frequenza

> La mia banda suona il rock
>
> ed è un'eterna partenza
>
> viaggia bene ad onde medie a
>
> modulazione di frequenza
>
> 
> *(Ivano Fossati, La mia banda suona il rock, 1979)*

La modulazione di frequenza, in sigla FM[^8], è la tecnica di modulazione maggiormente utilizzata per la trasmissione di informazioni utilizzando un segnale in radiofrequenza. La modulazione di frequenza consiste nel variare la frequenza del segnale che si intende utilizzare per la trasmissione (detto portante) in maniera proporzionale all'ampiezza del segnale che si intende trasmettere.

Rispetto alla modulazione di ampiezza, la modulazione di frequenza ha il vantaggio di essere meno sensibile ai disturbi e di permettere una trasmissione di miglior qualità. Ha inoltre una efficienza molto maggiore – rispetto alla modulazione di ampiezza – poiché la potenza del segnale modulato in FM è data esclusivamente dalla potenza della portante e non richiede potenza aggiuntiva per essere trasmesso. Invece nella modulazione AM la potenza trasmessa è funzione del segnale modulante, pertanto non può essere nota a priori.

Il difetto principale della modulazione di frequenza (ormai superato dalla tecnologia) è dato dalla necessità di realizzare circuiti più complessi, sia per la generazione del segnale da trasmettere, sia per la sua ricezione. Oggigiorno, questo punto viene posto decisamente in secondo piano poiché la tecnologia ha ormai costi irrisori, con il risultato che le trasmissioni in modulazione di ampiezza sono sempre meno usate soprattutto nell'ambito del *broadcasting*[^9].

### Teoria

Come nel caso della modulazione AM, per semplicità espositiva, supponiamo che il segnale modulante sia periodico, con pulsazione pari a $`\omega = 2\pi F`$. Premesso questo, si ha:

```math
v_m(t) = V_m \cos (\omega_m t + \varphi_m).
```

![Andamento del segnale modulante nel dominio del tempo.](https://gitlab.com/Giacaless/telecom/-/raw/master/img/modulazioni-analogiche/am-modulante.png)

Il segnale modulante di figura, per una più agevole semplicità dimostrativa, nel seguito verrà considerato con sfasamento nullo ($`\varphi_m = 0`$), ovvero in fase con in segnale portante. Una generalizzazione è semplice, ma – allo stadio attuale – appesantirebbe soltanto la spiegazione che pertanto viene così semplificata.

Per quanto riguarda il segnale portante – il quale deve avere una frequenza molto maggiore del segnale modulante – ovvero $`\omega_p \gg \omega_m`$, si avrà:

```math
v_p(t) = V_p \cos (\omega_p t).
```

![Andamento del segnale portante nel dominio del tempo.](https://gitlab.com/Giacaless/telecom/-/raw/master/img/modulazioni-analogiche/am-portante.png)

Come sottolineato nel caso della modulazione di ampiezza, il raffronto tra la il segnale modulante di figura e la rappresentazione del segnale portante, non evidenzia chiaramente cosa si intende per "molto maggiore". Le due immagini sono così riportate per motivi di chiarezza espositiva. È bene ribadire che per "molto maggiore" si intende una grandezza fisica di almeno tre o quattro ordini di grandezza superiore all'altra. Analogo ragionamento si esegue quando si parla di "molto minore".

> È importante notare che il punto di partenza di *ogni* modulazione è il medesimo: vi è un segnale modulante (l'informazione in banda base) e un segnale portante (l'intorno di frequenze in cui si collocherà l'informazione in banda traslata).
> Dopodiché, la diversa tecnologia di modulazione produrrà effetti differenti, ovvero diverse tipologie di segnali modulati.

Nel caso della modulazione FM la pulsazione di riposo del segnale modulato sarà pari a quella del segnale portante come indicato nell'equazione precedente, alla quale verrà sommato il segnale modulante moltiplicato per un'opportuna costante $`K_F`$ caratteristica del modulatore di frequenza.

Tale costante $`K_F`$ viene inserita per far sì che l'oscillazione in frequenza sia quella desiderata. Ne consegue che la pulsazione istantanea del segnale modulato in FM rispetta la seguente espressione: 

```math
\omega_{FM} = 
\begin{cases}
\omega_{p} + K_F v_m(t) = \\
= \omega_{p} + K_F V_m \cos \omega_m t = \\
= 2\pi \left( f_p + \frac{K_F V_m}{2\pi}\cos \omega_m t \right) = \\
= 2\pi \left( f_p + \Delta_f \cos \omega_m t \right)
\end{cases}
```

Dove, in quest'espressione, si è definita implicitamente $`\Delta_f`$ come *massimo scarto di frequenza*: 

```math
\Delta_f = \frac{K_F V_m}{2\pi}.
```

$`\Delta_f`$ viene definito massimo scarto di frequenza, rispetto alla frequenza di riposo $`f_p`$ corrispondente all'assenza di segnale modulante. Da quanto precede, si evince quanto segue: il segnale modulato è pari a un segnale cosinusoidale, avente frequenza $`f_p`$, la cui pulsazione è variabile rispetto al tempo. Pertanto si può scrivere (in termini generali):

```math
\omega(t)_{FM} = \frac{\text{d}\varphi(t)}{\text{d}t}.
```

Quest'espressione può essere riscritta in modo da esporre la fase $`\varphi(t)`$ la quale è l'argomento del segnale modulato[^10]. Dalla 

```math
\text{d}\varphi(t) = \omega_{FM}\text{d}t
```

si ha l'argomento del segnale modulato integrando ambo i membri rispetto al tempo e sostituendo nell'integrale la pulsazione istantanea del segnale modulato in FM. Pertanto si ha: 

```math
\varphi(t) = \int \omega_{FM}\text{d}t = 
\int \left( \omega_{p} + K_F V_m \cos \omega_m t \right)\text{d}t = 
\omega_{p} t + \frac{K_F V_m}{\omega_m} \sin \omega_m t.
```

Pertanto, il segnale modulato in FM, è dato da: 

```math
v_{FM}(t) = V_p \cos \left( \omega_{p} t + \frac{K_F V_m}{\omega_m} \sin \omega_m t \right) = V_p \cos \left( \omega_{p} t + m \sin \omega_m t \right).
```

Da sottolineare che in quest'espressione è stato definita la profondità di modulazione del segnale modulato in frequenza. Tale profondità (o indice) di modulazione è pari a: 

```math
m = \frac{K_F V_m}{\omega_m} = \frac{K_F V_m}{2\pi f_m} =\frac{\Delta_f}{f_m}.
```

La conoscenza della profondità di modulazione è molto importante anche nella modulazione di frequenza. In questo caso (a differenza della modulazione di ampiezza) indica quanto – nel segnale modulato – la frequenza del segnale portante varia, rispetto alla frequenza del segnale non modulato.

> Al contrario della modulazione AM, l'indice di modulazione $m$ per una modulazione FM può essere maggiore di 1 senza incorrere in sovramodulazione.

![Esempio di un segnale modulato in frequenza.](https://gitlab.com/Giacaless/telecom/-/raw/master/img/modulazioni-analogiche/fm-modulato.png)

In figura viene mostrato un esempio di un generico segnale modulante (in rosso), sovrapposto al segnale portante (in verde). Sotto, viene riportato il segnale modulato in frequenza (in blu).

Naturalmente, per evidenziare come si comporta il segnale modulato in frequenza, la pulsazione della portante $`\omega_p`$ è stata ridotta a livelli confrontabili con quella del segnale modulante $`\omega_m`$.

### Spettro di frequenza di un segnale FM

Per il calcolo della banda di un segnale modulato in FM, per prima cosa si applica addizione ai due argomenti del coseno: 

```math
v_{FM}(t) =
\begin{cases}
V_p \cos \left( \omega_{p} t + m \sin \omega_m t \right) = \\
= V_p \left(\cos(\omega_{p} t) \textcolor{red}{\underline{\textcolor{black}{\cos(m \sin \omega_m t)}}} - \sin(\omega_{p} t) \textcolor{red}{\underline{\textcolor{black}{\sin(m \sin \omega_m t)}}}\right)
\end{cases}.
```

Le espressioni sottolineate in rosso sono dette *funzioni di Bessel di prima specie* e vengono indicate con $`J_n(m)`$, dove l'indice $`n`$ rappresenta l'ordine della funzione di Bessel, mentre l'argomento $`m`$ il suo generico valore.

Perché è necessario l'utilizzo delle funzioni di Bessel per conoscere lo spettro di un segnale modulato in frequenza? Perché le espressioni sottolineate sono – rispettivamente – il coseno di un seno e il seno di un seno. Le funzioni di Bessel di prima specie consentono di eseguire il loro sviluppo in serie di Fourier.

Purtroppo nelle comuni calcolatrici scientifiche le funzioni di Bessel non sono implementate. Pertanto, è necessario utilizzare un foglio di calcolo qualsiasi (Excel, Libre Office, GoogleDocument non fa differenza).

![Funzione di Bessel di prima specie su Microsoft Excel](https://gitlab.com/Giacaless/telecom/-/raw/master/img/modulazioni-analogiche/bessel-excel.png)

Tra le innumerevoli funzioni a disposizione si troverà, nel caso di Excel, la funzione desiderata come mostrato in figura.

#### Esercizio *Calcolo del valore di $`J_n(m)`$ tramite foglio di calcolo*

Eseguire, tramite il foglio di calcolo, gli esercizi non svolti.

```math
\begin{aligned}
J_0(1) &= BESSEL.J(1;0) = 1 \\
J_3(0.4) &= BESSEL.J(0,4;3) = 0.0013 \\
J_2(7.45) &= BESSEL.J(7,45;2) = -0.2398 \\
J_8(7.45) &= BESSEL.J(7,45;8) = 0.1696 \\
J_0(5.520078) &= \\
J_1(1.8) &= \\
J_2(6.71) &= \\
J_3(5) &= \\
J_4(7) &= \\
J_5(8) &= \\
J_6(2) &= \\
J_7(3) &= \\
J_8(1.5) &= 
\end{aligned}
```

> L'esercizio $`J_0(5.520078)`$ non è stato inserito per creare panico o difficoltà nello studente. Rientra in una particolare categoria di valori detti *zeri di Bessel*, valori in cui le funzioni di Bessel si annullano, in questo caso per la funzione $`J_0(m)`$. Lo studio di questi valori è molto interessante poiché quando $`J_0(m) = 0`$ non vi è trasmissione della portante e il rendimento della modulazione è massimo, pari al $`\eta = 50 \%`$.

Pertanto, lo spettro di un generico segnale modulato in frequenza è composto da infinite righe distanziate tra di loro dalla frequenza del segnale modulante $`f_m`$ la cui ampiezza è data dall'ampiezza del segnale portante $`V_p`$ moltiplicato per la funzione di Bessel $`J_n(m)`$ dove $`m`$ è la profondità di modulazione, mentre $`n`$ è l'ennesima riga dello spettro.

È possibile dimostrare che lo sviluppo in serie di Bessel del segnale modulato in frequenza è pari a:

```math
\begin{array}{rl} v_{FM}(t) = & V_p J_0(m) \sin(\omega_p t) + \\ & V_p J_1(m) \left[ \sin(\omega_p + \omega_m)t - \sin(\omega_p - \omega_m)t \right] + \\ & V_p J_2(m) \left[ \sin(\omega_p + 2\omega_m)t + \sin(\omega_p - 2\omega_m)t \right] + \\ & V_p J_3(m) \left[ \sin(\omega_p + 3\omega_m)t - \sin(\omega_p - 3\omega_m)t \right] + \\ & V_p J_4(m) \left[ \sin(\omega_p + 4\omega_m)t + \sin(\omega_p - 4\omega_m)t \right] + \dots \end{array},
```

pertanto la banda di un segnale modulato in FM è illimitata. Il problema – in realtà – non sussiste in quanto le funzioni di Bessel di ordine elevato (per i valori di $`m`$ utilizzati nella modulazione di frequenza) assumono via, via valori sempre minori sino a non essere significative.

$`J_0(m)`$ è il valore che viene attribuito (a eccezione di $`V_p`$ che può essere considerato come un fattore di scala) al segnale portante. Quando tale valore si annulla, ovvero per $`J_0(m)=0`$ si ha rendimento massimo pari a $`\eta=50\%`$.

Questi valori prendono il nome di *zeri di Bessel* e – per la funzione $`J_0(m)`$ – sono riportati in tabella i primi cinque zeri di Bessel con una precisione alla quindicesima cifra decimale.

| Zero |         m        |
|:----:|:----------------:|
|  1°  | 2.40482555769577 |
|  2°  | 5.52007811028631 |
|  3°  | 8.65372791291101 |
|  4°  | 11.7915344390142 |
|  5°  | 14.9309177084877 |

![Funzioni di Bessel fino all'ordine 10 di prima specie](https://gitlab.com/Giacaless/telecom/-/raw/master/img/modulazioni-analogiche/bessel-J0-J10.png)

In figura sono riportati i grafici delle funzioni di Bessel fino al decimo ordine. Come evidente, la conoscenza dell'andamento di tali funzioni è propedeutica alla determinazione della banda del segnale modulato in FM.

Osservando che – nelle funzioni di Bessel – il valore di riferimento della portante non modulata è pari a $`J_0(0)=1`$, si stabilisce di considerare come facenti parte integrante della banda del segnale modulato in frequenza soltanto le armoniche le cui funzioni di Bessel – in corrispondenza al valore di $`m`$ prescelto – siano superiori, in modulo, a 0.01.

Mentre il matematico è rigoroso ed è interessato alla precisione assoluta, il tecnico utilizza il linguaggio della matematica e lo adatta ai propri fini. In questo caso, occorre capire quante armoniche sono necessarie per ottenere il segnale modulato come da specifiche di progetto una volta nota la profondità di modulazione $`m`$ e le armoniche con un'ampiezza significativa.

#### Esercizio *Calcolo della banda di un segnale modulato in FM*

Per lo studio dello spettro, l'insieme di tutte le armoniche che rappresentano il dominio della frequenza del segnale modulato, si ricorre a un esempio. Si vuole tracciare lo spettro di un segnale in modulazione di frequenza avente:

```math
\begin{aligned}
 & f_p = 102.5 \text{ MHz} \\
 & f_m = 15 \text{ kHz} \\
 & \Delta_f = 45 \text{ kHz} \\
 & V_p = 100 \text{ V}
\end{aligned}
```
Come primo passo si determina l'indice di modulazione $`m`$: 

```math
m = \frac{\Delta_f}{f_m} = \frac{45 \text{ kHz}}{15 \text{ kHz}} = 3.
```
Il passo successivo è individuare, sul diagramma delle funzioni di Bessel, un segmento parallelo all'asse delle ordinate in corrispondenza dell'indice di modulazione $`m=3`$ e, l'intersezione con tutte le curve $`J_0(3)`$, $`J_1(3)`$, $`J_2(3)`$,... per determinare i valori che queste funzioni assumono. Dal grafico (o dal foglio di calcolo) si ottengono i parametri di interesse per il calcolo dei termini significativi ai fini del calcolo della banda del segnale modulato da cui è immediato ricavare le ampiezze delle rispettive righe spettrali moltiplicando i valori ottenuti per la tensione della portante. 

```math
\left\{
            \begin{matrix}
                V_p J_0(3) = 100\text{ V} \cdot |-0.26|=26\text{ V} \\
                V_p J_1(3) = 100\text{ V} \cdot 0.34=34\text{ V} \\
                V_p J_2(3) = 100\text{ V} \cdot 0.48=48\text{ V} \\
                V_p J_3(3) = 100\text{ V} \cdot 0.32=32\text{ V} \\
                V_p J_4(3) = 100\text{ V} \cdot 0.12=12\text{ V} \\
                V_p J_5(3) = 100\text{ V} \cdot 0.05=5\text{ V} \\
                V_p J_6(3) = 100\text{ V} \cdot 0.01=1\text{ V}
            \end{matrix}
        \right.
```

Il termine $`J_6(3)=0.01`$ verrà escluso, come tutti i termini successivi. Pertanto, lo spettro del segnale modulato è pari a $`B=2\cdot5\cdot f_m = 2\cdot5\cdot15\text{ kHz}=150\text{ kHz}`$.


### Banda di Carson

In telecomunicazioni, la regola della *banda di Carson*[^11] definisce in modo approssimativo la larghezza di banda richiesta da un sistema di comunicazione per un segnale portante modulato in frequenza. La regola di Carson non si applica quando il segnale modulate contiene delle discontinuità, come un'onda quadra.

La larghezza di banda di Carson è espressa dalla relazione:

```math
B = 2\left( \Delta_f + f_m \right) = 2 f_m\left( m + 1 \right).
```

La regola della banda di Carson viene spesso applicata a trasmettitori, antenne, sorgenti ottiche, ricevitori e altri sistemi di telecomunicazioni.

Teoricamente ogni segnale FM ha una larghezza di banda illimitata, ma – all'atto pratico – la regola di Carson restituisce una larghezza di banda che copre il 98%, o oltre, della potenza dell'intero segnale modulato. Questa formula è tanto più esatta, quanto più $`m`$ è grande, mentre per valori di $`m`$ piccoli non risulta molto precisa.

## Note

[^8]: Dall'analogo termine inglese *Frequency Modulation*.
[^9]: Per *broadcasting* (o con l'obsoleto termine italiano *radioaudizioni circolari*) si intende la trasmissione di informazioni da un sistema trasmittente a un insieme di sistemi riceventi non definito a priori. L'esempio più classico è costituito da un trasmettitore radio di grande potenza e da un elevato numero di ricevitori montati nelle automobili o nelle case. In questo caso, tutti i ricevitori situati nell'area di copertura del trasmettitore riceveranno il segnale e il trasmettitore non potrà sapere esattamente con quanti ascoltatori ha comunicato.
[^10]: I passaggi matematici che seguono possono essere non noti, in quanto implicano la conoscenza della matematica infinitesimale (limiti, derivate, integrali e equazioni differenziali).
[^11]: La regola di Carson è riportata nel libro di John Renshaw Carson, *Notes on the theory of modulation*, Proc. IRE, vol. 10, no. 1 (Feb. 1922), pp. 57–64.