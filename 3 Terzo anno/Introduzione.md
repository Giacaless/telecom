# Introduzione

[[_TOC_]]
***

Per **modulazione** si intende la tecnologia che consente la trasmissione di un segnale elettromagnetico (detto *segnale modulante*) – mediante un generico mezzo trasmissivo – tramite un altro segnale elettromagnetico detto *segnale portante*. I segnali da modulare possono rappresentare le informazioni più diverse: audio, video, dati. Il risultato del processo di modulazione è il cosiddetto *segnale modulato*, dove il segnale modulante viene trasferito dalla sua *banda base* alla cosiddetta *banda traslata* (in un intorno della frequenza del segnale portante).

Il segnale modulante costituisce l’informazione da trasmettere, mentre il segnale portante non veicola informazione: serve esclusivamente per determinare in quali frequenze deve avvenire la modulazione stessa. Come accennato, il segnale modulato viene collocato dalla banda base, alla cosiddetta banda traslata, in un intorno di frequenze determinate da quelle occupate dal segnale portante.

> Per *segnale* si intende tutto ciò che costituisce informazione utile, non nota a priori. Con la dizione *non nota a priori*, si intende sottolineare che se un’informazione può essere ricavata (mediante un calcolo matematico o altro) allora è virtualmente nota, quindi utile solo a validare la correttezza e completezza della trasmissione dati.
>
> Alcuni esempi sono il codice fiscale, la partita IVA o il codice IBAN dove vi è una parte di contenuto informativo e una seconda parte deputata al controllo del corretto inserimento del codice stesso. Questa sezione, essendo ricavabile dalla prima mediante un algoritmo matematico, non aggiunge informazione e – pertanto – non viene considerata segnale: prende il nome di *ridondanza*.

L’operazione inversa di ripristino del segnale informativo originario in banda base è detta *demodulazione*. Il dispositivo in trasmissione che attua l’operazione di modulazione sul segnale informativo è detto *modulatore*, mentre il dispositivo in ricezione che attua l’operazione di demodulazione è detto demodulatore.

In un sistema di ricetrasmissione tali sistemi vengono riuniti entrambi sotto la dizione *modem* (dalla fusione dei termini *MOdulatore* e *DEModulatore*).

## Perché è conveniente modulare un segnale?

È conveniente – o meglio, necessario – modulare un segnale per molteplici ragioni.

Nel caso in cui i segnali debbano essere trasmessi mediante onde radio, il cosiddetto etere, l’antenna (sia in trasmissione, come in ricezione) deve avere un’altezza proporzionale alla lunghezza d’onda del segnale la quale – in banda base, per un segnale audio – è pari a circa:

```math
\lambda = \frac{c}{f} = \frac{2 \cdot 10^8 \text{ m/s}}{20 \text{ kHz}} \simeq 10 \text{ km}.
```

Modulare il segnale a frequenze più elevate riduce la lunghezza dell’antenna, rendendo il fenomeno della radiotrasmissione praticabile.

In secondo luogo, modulando più segnali a frequenze differenti, è possibile far transitare più informazioni in un unico mezzo trasmissivo, quindi – a titolo di esempio – più persone possono comunicare contemporaneamente senza generare tra loro interferenze, oppure occupare l’intero mezzo trasmissivo. Si noti che, parlando di generico “mezzo trasmissivo”, il discorso appena fatto vale sia per l’etere, ma anche per un generico cavo o qualsiasi altro supporto in grado di veicolare onde elettromagnetiche.

La modulazione è strategica poiché il segnale modulato può essere manipolato, al fine di ridurre gli effetti del rumore utilizzando le frequenze più opportune. Non tutte le modulazioni sono uguali e non tutte le frequenze hanno lo stesso tipo di comportamento. Per queste ragioni la modulazione, consentendo di ottimizzare gli effetti dei rumori, diventa il grimaldello per ridurre la quantità di banda di frequenza occupata a parità di qualità di trasmissione.

La natura del segnale stesso è tale da concentrare il suo spettro nelle frequenze più basse, mentre tutti i mezzi trasmissivi hanno un miglior rendimento e qualità a frequenze più elevate. Anche questo fatto depone a favore delle modulazioni.

Va anche detto che i segnali modulati possono essere a loro volta criptati, garantendo una maggior riservatezza durante la transazione dei dati. È cronaca di tutti i giorni quella relativa alle intercettazioni di ogni tipo. Una modulazione, specie se concepita *ad hoc*, consente di comunicare in sicurezza.

Si ha – infine, ma non certo per importanza – una semplificazione dei circuiti adottati per la trasmissione e la ricezione dei segnali.

## Tipologie di modulazione

Ogni segnale, è scomponibile mediante le sue armoniche e può essere riscritto mediante l’inviluppo di onde sinusoidali. Per questa operazione viene utilizzata l’analisi di Fourier quando il segnale è periodico. In questo caso l’analisi in frequenza del segnale presenta uno spettro a righe. Diversamente, per segnali non periodici, si ricorre alla trasformata di Fourier, poiché la loro analisi in frequenza, rivela uno spettro continuo.

![Spettro di un generico segnale discreto e continuo.](https://gitlab.com/Giacaless/telecom/-/raw/master/img/modulazioni-analogiche/spettro-generico.png)

Come visibile in figura, un segnale periodico presenta uno spettro a righe, dove nella Figura sono state ipotizzate cinque armoniche, dalla fondamentale $`A_1`$ fino alla quinta armonica $`A_5`$ avente frequenza pari a cinque volte quella della fondamentale.

Diversamente, un segnale non periodico mostra uno spettro continuo partendo dalla sua frequenza minima $`F_{min}`$ sino alla sua frequenza massima $`F_{max}`$.

Per queste ragioni, nel primo caso si parla di analisi di Fourier, dove la sintesi del segnale avviene attraverso la sommatoria delle sue armoniche. Diversamente, nel secondo caso si parla di trasformata di Fourier, dove la ricostruzione del segnale non può più avvenire tramite sommatorie delle sue singole componenti (infinitesime), ma soltanto attraverso un loro integrale[^1].

Pertanto, ogni componente in frequenza del segnale è di tipo sinusoidale (o cosinusoidale), indipendentemente dal fatto che il segnale sia periodico o non periodico. In generale ogni singola *armonica* è pari a:

```math
s(t) = \textcolor{red}{\underline{\textcolor{black}{A}}} \cdot \sin(\textcolor{red}{\underline{\textcolor{black}{\omega}}} t + \textcolor{red}{\underline{\textcolor{black}{\varphi}}}),
```

e, pertanto, per un segnale periodico si ottiene

```math
s(t) = \sum_{k=1}^{N}\textcolor{red}{\underline{\textcolor{black}{A_k}}} \cdot \sin(\textcolor{red}{\underline{\textcolor{black}{\omega_k}}} t + \textcolor{red}{\underline{\textcolor{black}{\varphi_k}}}),
```

dove i termini sottolineati corrispondono alle varie modulazioni che possono essere realizzate. Modulazione di ampiezza, frequenza e fase del generico segnale.

Da quanto precede, segue che possono esistere tre diverse tipologie di modulazione: la modulazione di ampiezza (agendo sull'ampiezza del segnale portante), la modulazione di frequenza (agendo sulla frequenza del segnale portante) e la modulazione di fase (agendo sulla fase del segnale portante).

Il segnale portante non contiene informazione, è una semplice sinusoide. La modulazione (di ampiezza, frequenza e fase) è il modo in cui il segnale portante insegue le generiche variazioni del segnale modulante.
