# Modulazioni analogiche (3)

[[_TOC_]]

***


## Modulazione di fase

La modulazione di fase (PM, *Phase Modulation*) è una modulazione che codifica le informazioni, come variazioni nella fase istantanea del segnale portante.

A differenza della più popolare modulazione di frequenza, la modulazione di fase non è molto diffusa in ambito analogico. Questo è dovuto al fatto che richiede circuiti più complessi, inoltre vi possono essere problemi di ambiguità nel determinare se – per esempio – il segnale è cambiato dalla fase di $`+180^\circ`$ o $`-180^\circ`$.

Tuttavia, la modulazione di fase è ampiamente utilizzata per trasmettere onde radio ed è parte integrante di molti schemi di codifica di trasmissione digitale che sono alla base di una vasta gamma di tecnologie come il WiFi, GSM e la televisione satellitare.

La modulazione di fase è strettamente correlata alla modulazione di frequenza (FM); viene spesso utilizzata come passo intermedio per ottenere la modulazione FM. Matematicamente parlando, la modulazione di fase e di frequenza possono essere considerate un caso particolare di modulazione in quadratura di ampiezza (QAM, *Quadrature Amplitude Modulation*) e – in molti testi – vengono definite *modulazioni angolari*.

### Teoria

Per semplicità, supponiamo che il segnale modulante sia periodico, con pulsazione pari a $`\omega = 2\pi F`$. Premesso questo, come nei casi delle modulazioni AM e FM, si ha: 

```math
    v_m(t) = V_m \cos (\omega_m t + \varphi_m),
```

dove, per una più agevole semplicità dimostrativa, nel seguito verrà considerato con $`\varphi_m = 0`$, ovvero in fase con in segnale portante.

Per quanto riguarda il segnale portante – il quale deve avere una frequenza molto maggiore del segnale modulante – ovvero $`\omega_p \gg \omega_m`$, si avrà: 

```math
    v_p(t) = V_p \cos (\omega_p t).
```

Premesso questo, il segnale modulato sarà pari al segnale portante sfasato in ragione del segnale modulante, ovvero: 

```math
v_{PM}(t) = V_p \cos \left(\omega_p t + v_m(t) \right).
```

Ciò dimostra come $`v_m(t)`$ moduli la fase. Ovviamente, la modulazione di fase può anche essere vista come un cambiamento della frequenza della portante. Pertanto, la modulazione di fase può quindi essere considerata un caso particolare della modulazione di frequenza, dove la frequenza della portante è data dalla derivata rispetto al tempo della modulazione di fase.

Il comportamento spettrale della modulazione di fase non è semplice da ricavare, ma la matematica rivela che ci sono due casi di particolare interesse:

* per piccoli segnali, la modulazione di fase è analoga alla modulazione di ampiezza, presentando il raddoppio della larghezza di banda e – di conseguenza – uno scarso rendimento;
* nel caso di un singolo grande segnale sinusoidale, la modulazione di fase è invece simile alla modulazione di frequenza e la sua larghezza di banda è pari a circa $`B = 2(h+1)f_m`$, dove $`f_m = \frac{\omega_m}{2\pi}`$, mentre $`h`$ è l'*indice di modulazione* che verrà definito tra breve. Tutto questo è noto come *Regola di Carson per la modulazione di fase*.

### Indice di modulazione

Come visto con gli indici di modulazione di ampiezza e frequenza, questa grandezza indica quanto la variabile modulata vari intorno al suo livello non modulato. È relativo alle variazioni di fase della portante del segnale, pertanto si ha:

```math
h = \Delta\varphi,
```

dove $`\Delta\varphi`$ è il picco della variazione di fase.

È istruttivo confrontare questo indice di modulazione con l'indice di modulazione ottenuto nel caso della modulazione in frequenza.